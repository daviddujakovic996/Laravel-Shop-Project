<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kontakt;

class PorukeController extends FrontController
{
    public function updateStatus(Request $request){
    $model=new Kontakt();
   
    $naslov=$request->input("naslov");
    $poruke=$model->updateStatus($naslov);
  
    
      if($poruke){
          return response(["uspesan update"], 201);
      }else{
        return response(["neuspesan update"], 400);
     }
    }
}
