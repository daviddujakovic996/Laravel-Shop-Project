<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patike;

class PatikeController extends FrontController
{
    public function filterData(Request $request){
        $mincena=0;
        $maxcena=10000;
        $brend=0;
        $pol=0;
        $godina=0;
        $start=1;
        $opcije=0;
        $search="";

        if($request->has("minimum_price") && $request->has("maximum_price"))
        {
         $mincena=$request->input("minimum_price");
         $maxcena=$request->input("maximum_price");
        }

        if($request->has("brend"))
        {
        $brend = implode("','", $request->input("brend"));
        }

        if($request->has("pol"))
        {
        $pol = implode("','", $request->input("pol"));
        }

        if($request->has("godina"))
        {
        $godina = implode("','", $request->input("godina"));
        }
        if($request->has("opcije"))
        {
        $opcije = $request->input("opcije");
        }
        if($request->has("search"))
        {
        $search = $request->input("search");
        
        }
        
        $brojPoStrani=6;
        $request->has("strana") ? $strana=$request->input("strana") : $strana=0;
						
						if($strana>0){
							$start=($strana*$brojPoStrani)-$brojPoStrani;				
						}else{
							$start=0;
						}


        $model=new Patike();
        $rezultati=$model->filterData($mincena,$maxcena,$brend,$pol,$godina,$start,$brojPoStrani,$opcije,$search);
        $broj=$model->prebrojSvePatike($mincena,$maxcena,$brend,$pol,$godina,$opcije,$search);
        array_push($rezultati,$broj);
        if($rezultati){
            return response([$rezultati],201);
        }else{
          return response(["problem!"], 400);
       }
    }
}
