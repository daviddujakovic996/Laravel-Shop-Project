<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Korpa;

class KorpaController extends BackendController
{
    public function accept(Request $request){
        $idKorpa=$request->input("korpa");

        $model=new Korpa();
        $rez=$model->prihvati($idKorpa);
        if($rez){
            return \redirect("/adminPanel/korpa")->with("message", "Uspesno ste prihvatili narudzbu!");
        } else {
           return redirect("/adminPanel/korpa")->with("message", "Niste uspeli da prihvatite narudzbu!");
        }
    }
    public function denied(Request $request){
        $idKorpa=$request->input("korpa");

        $model=new Korpa();
        $rez=$model->odbij($idKorpa);
        if($rez){
            return \redirect("/adminPanel/korpa")->with("message", "Uspesno ste odbili narudzbu!");
        } else {
           return redirect("/adminPanel/korpa")->with("message", "Niste uspeli da odbijete narudzbu!");
        }
    }
}
