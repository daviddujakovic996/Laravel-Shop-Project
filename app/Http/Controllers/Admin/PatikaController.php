<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PatikaRequest;
use App\Models\Cena;
use App\Models\Patike;
use App\Models\Slika;
use App\Models\GodinaIzdanja;
use App\Models\Brend;
use App\Http\Requests\GodinaRequest;
use App\Http\Requests\BrendRequest;

class PatikaController extends BackendController
{
    public function insert(PatikaRequest $request){
        if($request->has("image")){
            $imageName = time().'.'.$request->image->extension();  
            $request->image->move(public_path('img/bg-img'), $imageName);
            }else{
                $imageName="1.jpg";
            }

        $naziv=$request->input("naziv");
        $cena=$request->input("cena");
        if($request->has("popust")){
            $popust=$request->input("popust");
        }else{
            $popust=0;
        };
        $brend=$request->input("brend");
        $pol=$request->input("pol");
        $godina=$request->input("godinaIzdanja");

        $modelCena=new Cena();
        $idCena=$modelCena->insertCena($cena,$popust);

        $modelSlika=new Slika();
        $idSlika=$modelSlika->insertSlika($imageName);

        $modelPatika=new Patike();
        $rezultat=$modelPatika->insertPatika($brend,$naziv,$pol,$godina,$idCena,$idSlika);
        
        if($rezultat){
       
            return \redirect("/adminPanel/patike/dodaj")->with("message", "Uspesno ste ubacili novi proizvod!");
        } else {
           return redirect("/adminPanel/patike/dodaj")->with("message", "Niste uspeli da ubacite proizvod , proverite sva polja!");
            
        }
    }

    public function update(PatikaRequest $request){
        if($request->has("image")){
        $imageName = time().'.'.$request->image->extension();  
        $request->image->move(public_path('img/bg-img'), $imageName);
        }else{
            $imageName=$request->input("defaultSlika");
        }
        $idSlikaa=$request->input("idSlika");
        $idCenaa=$request->input("idCena");
        $idPatika=$request->input("idPatika");

        $naziv=$request->input("naziv");
        $cena=$request->input("cena");
        $popust=$request->input("popust");
        $brend=$request->input("brend");
        $pol=$request->input("pol");
        $godina=$request->input("godinaIzdanja");

        $modelCena=new Cena();
        $cena=$modelCena->updateCena($cena,$popust,$idCenaa);

        $modelSlika=new Slika();
        $slika=$modelSlika->updateSlika($imageName,$idSlikaa);

        $modelPatika=new Patike();
        $rezultat=$modelPatika->updatePatika($brend,$naziv,$pol,$godina,$idPatika);
        
        if($rezultat){
       
            return \redirect("/adminPanel/patike")->with("message", "Uspesno ste izmenili proizvod!");
        } else {
           return redirect("/adminPanel/patike")->with("message", "Niste uspeli da izmenite proizvod , proverite sva polja!");
            
        }
    }

    public function delete($id=0){
       
        $model=new Patike();
        $patika=$model->deletePatika($id);
        if($patika){
       
            return \redirect("/adminPanel/patike")->with("message", "Uspesno ste obrisali proizvod!");
        } else {
           return redirect("/adminPanel/patike")->with("message", "Niste uspeli da obrisete proizvod , proverite sva polja!");
            
        }
    }

    public function obrisiGodina($id=0){
        $model=new GodinaIzdanja();
        $godina=$model->deleteGodina($id);
        if($godina){
       
            return \redirect("/adminPanel/godinaIzdanja")->with("message", "Uspesno ste obrisali godinu!");
        } else {
           return redirect("/adminPanel/godinaIzdanja")->with("message", "Niste uspeli da obrisete godinu , proverite sva polja!");
            
        }
    }

    public function unesiGodinu(GodinaRequest $request){
        $naziv=$request->input("godina");
        $model=new GodinaIzdanja();
        $godina=$model->isertGodina($naziv);
        if($godina){
       
            return \redirect("/adminPanel/godinaIzdanja")->with("message", "Uspesno ste uneli godinu!");
        } else {
           return redirect("/adminPanel/godinaIzdanja")->with("message", "Niste uspeli da unesete godinu , proverite sva polja!");
            
        }
    }

    public function unesiBrend(BrendRequest $request){
        $naziv=$request->input("brend");
        $model=new Brend();
        $brend=$model->insertBrend($naziv);
        if($brend){
       
            return \redirect("/adminPanel/brend")->with("message", "Uspesno ste uneli brend!");
        } else {
           return redirect("/adminPanel/brend")->with("message", "Niste uspeli da unesete brend , proverite sva polja!");
            
        }
    }
    public function obrisiBrend($id=0){
        $model=new Brend();
        $brend=$model->deleteBrend($id);
        if($brend){
       
            return \redirect("/adminPanel/brend")->with("message", "Uspesno ste obrisali brend!");
        } else {
           return redirect("/adminPanel/brend")->with("message", "Niste uspeli da obrisete brend , proverite sva polja!");
            
        }
    }

    public function izmeniBrend(BrendRequest $request){
        $naziv=$request->input("brend");
        $id=$request->input("idbrend");
        $model=new Brend();
        $brend=$model->updateBrend($naziv,$id);
        if($brend){
       
            return \redirect("/adminPanel/brend/izmeni/$id")->with("message", "Uspesno ste izmenili brend!");
        } else {
           return redirect("/adminPanel/brend/izmeni/$id")->with("message", "Niste uspeli da izmenite brend , proverite sva polja!");
            
        }
    }
}
