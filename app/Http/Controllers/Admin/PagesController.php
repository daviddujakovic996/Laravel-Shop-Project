<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kontakt;
use App\Models\Korisnik;
use App\Models\Patike;
use App\Models\Pol;
use App\Models\Brend;
use App\Models\GodinaIzdanja;
use App\Models\Korpa;
use App\Models\Aktivnosti;

class PagesController extends BackendController
{
    public function kontakt()
    {
        $model = new Kontakt();
        $korisnik=session("korisnik")->idKorisnik;
        $poruke = $model->getMessageForOneUser($korisnik);
        if($poruke){
            $this->data["poruke"]=$poruke;
            
        }
    }
    public function home(){
        $this->kontakt();

        $korisniciModel=new Korisnik();
        $rezultati=$korisniciModel->getAll();
        $this->data["korisnici"]=$rezultati;
        return view("pages.admin.home", $this->data);

    }
    public function mojNalog(){
        $this->kontakt();
        return view("pages.admin.mojNalog", $this->data);
    }
    public function patike(){
        $this->kontakt();
        $model=new Patike();
        $patike=$model->getAll();
        $this->data["patike"]=$patike;
        return view("pages.admin.patike", $this->data);
    }

    public function dodajProizvod(){
        $this->kontakt();
        $pol=new Pol();
        $polRez=$pol->getAll();
        $this->data["pol"]=$polRez;

        $brend=new Brend();
        $brendRez=$brend->getAll();
        $this->data["brend"]=$brendRez;

        
        $godina=new GodinaIzdanja();
        $godinarez=$godina->getAll();
        $this->data["godina"]=$godinarez;
        return view("pages.admin.dodajProizvod", $this->data);
    }
    public function izmeniProizvod($id=0){
        $this->kontakt();
        $pol=new Pol();
        $polRez=$pol->getAll();
        $this->data["pol"]=$polRez;

        $brend=new Brend();
        $brendRez=$brend->getAll();
        $this->data["brend"]=$brendRez;

        
        $godina=new GodinaIzdanja();
        $godinarez=$godina->getAll();
        $this->data["godina"]=$godinarez;
        $model=new Patike();
        $patika=$model->getOne($id);
        $this->data["patika"]=$patika;
        return view("pages.admin.izmeniProizvod", $this->data);
    }
    public function godina(){
        $this->kontakt();
        
        $godina=new GodinaIzdanja();
        $godinarez=$godina->getAll();
        $this->data["godina"]=$godinarez;
        return view("pages.admin.godina", $this->data);
    }
    public function brend(){
        $this->kontakt();
        
        $model=new Brend();
        $brend=$model->getAll();
        $this->data["brend"]=$brend;
        return view("pages.admin.brend", $this->data);
    }
    public function izmeniBrend($id=0){
        $this->kontakt();
        
        $model=new Brend();
        $brend=$model->getOne($id);
        $this->data["brend"]=$brend;
        return view("pages.admin.izmeniBrend", $this->data);
    }

    public function korpa(){
        $this->kontakt();
        
        $model=new Korpa();
        $korpa=$model->dohvatiSve();
        $this->data["korpa"]=$korpa;
        return view("pages.admin.korpa", $this->data);
    }

    public function poruke(){
        $this->kontakt();
        return view("pages.admin.poruke", $this->data);
    }
    public function odgovori($email=0,$id=0){
        $this->kontakt();
       $this->data["email"]=$email;
       $this->data["idPorukee"]=$id;
        return view("pages.admin.odgovor", $this->data);
    }

    public function aktivnost(){
        $this->kontakt();

        $akModel=new Aktivnosti();
        $rez=$akModel->getAll();
        $this->data["aktivnost"]=$rez;
        return view("pages.admin.aktivnost", $this->data);
    }
}
