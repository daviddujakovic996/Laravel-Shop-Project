<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PasswordRequest;
use App\Models\Korisnik;

class AuthController extends BackendController
{
    public function passwordChange(PasswordRequest $request){
        $password = $request->input("password");
        $md5password=md5($password);
        $newpassword = $request->input("newpassword");
        $renewpassword = $request->input("renewpassword");
        if($newpassword==$renewpassword){
            $md5newpassword=md5($newpassword);
        }else{
            return redirect("/adminPanel/mojNalog")->with("message", "Nisu vam iste lozinke!");
        }

        $model = new Korisnik();
        $korisnik=session("korisnik")->idKorisnik;
        $korisnikkk = $model->updatePassword($korisnik, $md5password,$md5newpassword);

        if($korisnikkk){
         

            return \redirect("/adminPanel/mojNalog")->with("message", "Uspesno ste promenili lozinku!");
        } else {
            return redirect("/adminPanel/mojNalog")->with("message", "Niste uspeli da promenite lozinku!");
        }
    }
}
