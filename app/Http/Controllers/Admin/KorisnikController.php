<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Korisnik;
use App\Models\Korpa;
use App\Models\Kontakt;

class KorisnikController extends BackendController
{
    public function deleteUser(Request $request){
        $korisnik=$request->input("obrisi");
        $modelKorpa=new Korpa();
        $rezultat=$modelKorpa->deleteUser($korisnik);
        $modelKontakt=new Kontakt();
        $rezultatKo=$modelKontakt->deleteUser($korisnik);
        $modelKorisnik=new Korisnik();
        $rezultatKorisnik=$modelKorisnik->deleteUser($korisnik);
        if($rezultat || $rezultatKo || $rezultatKorisnik){
            return \redirect("/adminPanel")->with("message", "Uspesno ste obrisali korisnika!");
        } else {
           return redirect("/adminPanel")->with("message", "Niste uspeli obrisati korisnika!");
        }
    }
}
