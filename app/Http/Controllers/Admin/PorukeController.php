<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kontakt;
use App\Models\Korisnik;
use App\Http\Requests\OdgovorRequest;


class PorukeController extends BackendController
{
    public function odgovori(OdgovorRequest $request){
        $model=new Korisnik();
   
        $naslov=$request->input("naslov");
        
        $email=$request->input("emailskr");
        $poruka=$request->input("tekst");
        $idskr=$request->input("idskr");
        $user=$model->getId($email);
  
        $kontakt=new Kontakt();
        if($user){
            
            $messageInsert=$kontakt->unesiPoruku($naslov,$user->idKorisnik,$poruka);
            $messageUpdate=$kontakt->izmeniStatus($idskr);
            return \redirect("/adminPanel/poruke")->with("message", "Uspesno ste odgovorili korisniku!");
        } else {
            $messageUpdate=$kontakt->izmeniStatus($idskr);
            $headers="From:admin@hotmail.com";
           mail($email,$naslov,$poruka,$headers);
           return redirect("/adminPanel/poruke")->with("message", "Uspesno poslat mejl korisniku!");
            
        }
    }
    }

