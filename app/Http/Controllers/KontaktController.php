<?php

namespace App\Http\Controllers;

use App\Http\Requests\KontaktRequest;
use Illuminate\Http\Request;
use App\Models\Kontakt;

class KontaktController extends FrontController
{
    public function kontaktAdmin(KontaktRequest $request){

    $email = $request->input("email");
    $naslov = $request->input("naslov");
    $tekst = $request->input("tekst");

    $model = new Kontakt();
    
    $kontakt = $model->insertMessage($email,$naslov,$tekst);
        
    if($kontakt){
       
        return \redirect("/Kontakt")->with("message", "Uspesno ste poslali poruku,ocekujte odgovor!");
    } else {
       return redirect("/Kontakt")->with("message", "Proverita sva polja jos jednom!");
        
    }
    
    }

    
}
