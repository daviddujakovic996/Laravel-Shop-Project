<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\PasswordRequest;
use App\Models\Korisnik;

class AuthController extends FrontController
{
    public function register(RegisterRequest $request){
       
        $ime = $request->input("first_name");
        $prezime = $request->input("last_name");
        $email = $request->input("email");
        $lozinka = md5($request->input("password"));
        $reLozinka=md5($request->input("passwordrepeat"));
       
        if($lozinka==$reLozinka){
            $sifra=$lozinka;
        }
     
        $imePrezime=$ime . " " . $prezime;
        $model = new Korisnik();
        $korisnik = $model->insertUser($imePrezime,$email, $sifra);
      if($korisnik){
          return response(["uspesno"], 201);
      }else{
        return response(["postoji takav korisnik"], 400);
     }
        
    }

    public function login(LoginRequest $request){
        $email = $request->input("email");
        $lozinka = md5($request->input("password"));

        $model = new Korisnik();
        $korisnik = $model->getByEmailAndPassword($email, $lozinka);

        if($korisnik){
         
            $request->session()->put("korisnik", $korisnik);

            
            return \redirect("/")->with("message", "Uspesno ste se ulogovali!");
        } else {
            return redirect("/login")->with("message", "Niste registrovani!");
        }
    }

    
    public function logout(Request $request){
        $request->session()->forget("korisnik");
        $request->session()->flush(); // = destroy()
        return redirect("/login")->with("message", "Izlogovali ste se");
    }

    public function passwordChange(PasswordRequest $request){
        $password = $request->input("password");
        $md5password=md5($password);
        $newpassword = $request->input("newpassword");
        $renewpassword = $request->input("renewpassword");
        if($newpassword==$renewpassword){
            $md5newpassword=md5($newpassword);
        }else{
            return redirect("/Moj Nalog")->with("message", "Nisu vam iste lozinke!");
        }

        $model = new Korisnik();
        $korisnik=session("korisnik")->idKorisnik;
        $korisnikkk = $model->updatePassword($korisnik, $md5password,$md5newpassword);

        if($korisnikkk){
         

            return \redirect("/Moj Nalog")->with("message", "Uspesno ste promenili lozinku!");
        } else {
            return redirect("/Moj Nalog")->with("message", "Niste uspeli da promenite lozinku!");
        }
    }
}
