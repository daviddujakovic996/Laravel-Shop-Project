<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;


class FrontController extends Controller
{
    protected $data = [];

    public function  __construct()
    {
        $model = new Menu();
        $this->data["menu"] = $model->getAll();
        
    }
}
