<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Korpa;

class KorpaController extends FrontController
{
    public function dodajPatikuUKorpu(Request $request){
        $patika = $request->input("patika");
        $cena = $request->input("cena");
        $kolicina = $request->input("quantity");
        $korisnik=session("korisnik")->idKorisnik;
        $ukupnaCena=(int)$cena*(int)$kolicina;

        $model = new Korpa();
        
        $dupliranjePodataka=$model->duplicateRows($korisnik, $patika);
        if(!$dupliranjePodataka){
        $ubaciUKorpu = $model->InsertIntoCart($korisnik, $patika,$kolicina,$ukupnaCena);

        if($ubaciUKorpu){
         

            return \redirect("/patika/$patika")->with("message", "Uspesno ste ubacili proizvod u korpu!");
        } else {
            return redirect("/patika/$patika")->with("message", "Niste uspeli da ubacite proizvod u korpu!");
        }

        }else{
            return redirect("/patika/$patika")->with("message", "Vec ste ubacili ovakav proizvod u korpu!");
        }
    }

    public function obrisiIzKorpeProizvod(Request $request){
        $korpa = $request->input("proizvod");
        
        $model = new Korpa();
        
        $obrisiIzKorpe=$model->deleteFromCartProduct($korpa);
        if($obrisiIzKorpe){
            return \redirect("/korpa")->with("poruka", "Uspesno ste obrisali proizvod iz korpe!");
        } else {
            return redirect("/korpa")->with("poruka", "Niste uspeli da obrisete proizvod iz korpe!");
        }
        
    }

    public function naruciProizvode(Request $request){
        $korisnik = session("korisnik")->idKorisnik;
        
        $model = new Korpa();
        
        $naruci=$model->updateStatusKorpa($korisnik);
        if($naruci){
            return \redirect("/korpa")->with("message", "Uspesno ste narucili proizvode iz korpe!");
        } else {
            return redirect("/korpa")->with("message", "Niste uspeli da narucite proizvode iz korpe!");
        }
    }
}
