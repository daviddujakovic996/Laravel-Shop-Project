<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patike;
use App\Models\Kontakt;
use App\Models\Korpa;
use App\Models\Pol;
use App\Models\Brend;
use App\Models\GodinaIzdanja;

class PagesController extends FrontController
{
    
    public function home(){
        $model=new Patike();
        $patike=$model->getNineWithDiscout();

        $this->data["patike"]=$patike;
        return view("pages.front.home", $this->data);
    }

    public function register(){
        return view("pages.front.registracija",$this->data);
    }
    public function login(){
        return view("pages.front.login",$this->data);
    }
    public function kontakt(){
        return view("pages.front.kontakt",$this->data);
    }
    public function mojNalog(){
        $model=new Kontakt();
        $korisnik=session("korisnik")->idKorisnik;
        $poruke=$model->getMessageForOneUser($korisnik);
        if($poruke){
            $this->data["poruke"]=$poruke;
            
        }
        $modelKorpa=new Korpa();
        $korpa=$modelKorpa->getAccept($korisnik);

        $this->data["sviProizvodiUKorpi"]=$korpa;
        return view("pages.front.mojNalog",$this->data);
    }

    public function patika($id=0){
        $model=new Patike();
        $patike=$model->getOne($id);

        $this->data["patika"]=$patike;
        return view("pages.front.patika",$this->data);
    }
    public function korpa(){
        $korisnik=session("korisnik")->idKorisnik;
        $model=new Korpa();
        $korpa=$model->getAll($korisnik);

        $this->data["sviProizvodiUKorpi"]=$korpa;
        return view("pages.front.korpa",$this->data);
    }

    public function shop(){
        $model=new Patike();
        $patike=$model->getFirstNine();
        $this->data["patike"]=$patike;

        $pol=new Pol();
        $polRez=$pol->getAll();
        $this->data["pol"]=$polRez;

        $brend=new Brend();
        $brendRez=$brend->getAll();
        $this->data["brend"]=$brendRez;

        
        $godina=new GodinaIzdanja();
        $godinarez=$godina->getAll();
        $this->data["godina"]=$godinarez;

        return view("pages.front.shop", $this->data);
    }
}
