<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Aktivnosti;

class aktivnost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $model=new Aktivnosti();
        $putanja=$request->path();
        $korisnik=$request->ip();
        $aktivnost=$model->insert($putanja,$korisnik);
        return $next($request);
    }
}
