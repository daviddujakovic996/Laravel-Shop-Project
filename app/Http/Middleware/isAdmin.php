<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has("korisnik")){
            $user = $request->session()->get("korisnik");
            // dd($user);

            if($user->NazivUloge == "Admin"){
                return redirect("/admin")->with("message", "Zdravo admine!!");
            }else{
                return redirect("/")->with("message", "Nemate pristup!");
            }
        }
        
    }
}
