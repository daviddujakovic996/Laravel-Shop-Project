<?php

namespace App\Http\Middleware;

use Closure;

class isLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has("korisnik")){
            $user = $request->session()->get("korisnik");
            // dd($user);

            if($user->NazivUloge == "Korisnik" || $user->NazivUloge == "Admin"){
                return redirect("/")->with("message", "NEMATE PRISTUP!!");
            }
        }
        return $next($request);
    }
}
