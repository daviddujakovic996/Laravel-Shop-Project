<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name"=>"required|regex:/^[A-Z][a-z]{2,}$/",
            "last_name"=>"required|regex:/^[A-Z][a-z]{2,}$/",
            "email" => "required|unique:korisnici,Email|email",
            "password" => [ "required", "regex:/^[A-z\d]{4,}$/" ],
            "passwordrepeat" => [ "required", "regex:/^[A-z\d]{4,}$/" ]
        ];
    }
    public function message(){
        return [
            "required" => "Polje je obavezno!"
        ];
    }
}
