<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatikaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'naziv'=>[ "required", "regex:/^[A-z\d\s]+$/" ],
            'cena'=>'required',
            'brend'=>'required',
            'pol'=>'required',
            'godinaIzdanja'=>'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ];
    }
}
