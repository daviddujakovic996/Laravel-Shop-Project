<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kontakt extends Model
{
    public function insertMessage($email,$naslov,$tekst){
        return \DB::table("kontakt")
        ->insert(
        ['FromEmail' => $email, 'Naslov' => $naslov, 'Poruka'=>$tekst , "ToEmail"=>
        (int)(\DB::select("SELECT idKorisnik FROM korisnici WHERE email='admin@hotmail.com'"))
        ]
    );
    }
    public function unesiPoruku($naslov,$email,$poruka){
        return \DB::table("kontakt")
        ->insert(
        ['FromEmail' => "admin@hotmail.com", 'Naslov' => $naslov, 'Poruka'=>$poruka , "ToEmail"=>$email
        ]
        );
    }
    public function getMessageForOneUser($korisnik){
        return \DB::table("kontakt")
        ->select("*")
        ->where(["ToEmail"=>(int)$korisnik])
        ->get();

    }
    public function updateStatus($naslov){
        return \DB::table("kontakt")
        ->where(
            ["Naslov"=>$naslov]
        )
        ->update(['status' => 1]);
       

    }
    public function izmeniStatus($id){
        return \DB::table("kontakt")
        ->where(
            ["idPoruka"=>$id]
        )
        ->update(['status' => 1]);
       

    }
    public function deleteUser($korisnik){
        return \DB::table('kontakt')->where('ToEmail', '=', $korisnik)->delete();
    }
 

    
  
}
