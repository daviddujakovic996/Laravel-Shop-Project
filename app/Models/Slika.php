<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slika extends Model
{
    public function insertSlika($slika){
        $id= \DB::table("slika")
            ->insertGetId(
                ['Putanja' => $slika]
            );

        return $id;
    }

    public function updateSlika($slika,$id){
        return \DB::table("slika")->where(
            ['idSlika' => $id]
        )
            ->update(
                ['Putanja' => $slika]
            );

    }
}
