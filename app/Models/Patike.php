<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patike extends Model
{
    public function getNineWithDiscout(){
        return \DB::table("patike as p")
            ->join("cena as c","p.idCena","=","c.idCena")
            ->join("slika as s","p.idSlika","=","s.idSlika")
            ->select("p.idPatika","p.Naziv","c.originalnaCena","c.popust","s.Putanja","s.NazivSlike")
            ->where([
                ["c.popust",">","0"]
            ])
            ->orderBy("c.popust","desc")
            ->limit(9)
            ->get();
    }
    public function getOne($id){
        return \DB::table("patike as p")
            ->join("cena as c","p.idCena","=","c.idCena")
            ->join("slika as s","p.idSlika","=","s.idSlika")
            ->join("pol","p.idPol","=","pol.idPol")
            ->join("brend as b","p.idBrend","=","b.idBrend")
            ->join("godinaizdanja as gi","p.idGodinaIzdanja","=","gi.idGodinaIzdanja")
            ->select("p.idPatika","p.Naziv","b.Naziv as brend","b.idBrend","c.idCena","c.originalnaCena","c.popust","s.idSlika","s.Putanja","s.NazivSlike","pol.Naziv as poll","pol.idPol","gi.godinaIzdanja","gi.idGodinaIzdanja")
            ->where([
                ["p.idPatika","=",$id]
            ])
            ->limit(1)
            ->get();
    }
    public function getFirstNine(){
        return \DB::table("patike as p")
            ->join("cena as c","p.idCena","=","c.idCena")
            ->join("slika as s","p.idSlika","=","s.idSlika")
            ->select("p.idPatika","p.Naziv","c.originalnaCena","c.popust","s.Putanja","s.NazivSlike")
            ->orderBy("p.idPatika","desc")
            ->limit(8)
            ->get();
    }

    public function filterData($mincena,$maxcena,$brend,$pol,$godina,$start,$brojPoStrani,$opcije,$search){
        $query = "
  SELECT p.idPatika,p.Naziv,s.Putanja,c.originalnaCena,c.popust FROM patike p INNER JOIN slika s ON p.idSlika=s.idSlika INNER JOIN godinaizdanja g ON p.idGodinaIzdanja=g.idGodinaIzdanja INNER JOIN cena c ON p.idCena=c.idCena WHERE status = '0'
 ";
        if($mincena!=0 || $maxcena!=0){
            $query .= "
            AND (c.originalnaCena-c.popust) BETWEEN '$mincena' AND '$maxcena'
            ";
        }
        if($brend!=0){
            $query .= " 
            AND p.idBrend IN('$brend')
             ";
        }
        if($pol!=0){
            $query .= " 
            AND p.idPol IN('$pol')
             ";
        }
        if($godina!=0){
            $query .= "
            AND p.idGodinaIzdanja IN('$godina')
            ";
        }
        if($search!=""){
            $query .= "
            AND p.Naziv LIKE '%" . $search . "%' 
            ";
        }
        if($opcije==0){
        $query.=" ORDER BY g.godinaIzdanja DESC LIMIT $start,$brojPoStrani";
        }elseif($opcije==1){
        $query.=" ORDER BY g.godinaIzdanja ASC LIMIT $start,$brojPoStrani";    
        }elseif($opcije==2){
            $query.=" ORDER BY (c.originalnaCena-c.popust) DESC LIMIT $start,$brojPoStrani";    
        }elseif($opcije==3){
            $query.=" ORDER BY (c.originalnaCena-c.popust) ASC LIMIT $start,$brojPoStrani";    
            }
        return \DB::select($query);
    }

    public function prebrojSvePatike($mincena,$maxcena,$brend,$pol,$godina,$opcije,$search){
        $query = "
  SELECT COUNT(p.idPatika) as broj FROM patike p INNER JOIN slika s ON p.idSlika=s.idSlika INNER JOIN godinaizdanja g ON p.idGodinaIzdanja=g.idGodinaIzdanja INNER JOIN cena c ON p.idCena=c.idCena WHERE status = '0'
 ";
        if($mincena!=0 || $maxcena!=0){
            $query .= "
            AND (c.originalnaCena-c.popust) BETWEEN '$mincena' AND '$maxcena'
            ";
        }
        if($brend!=0){
            $query .= " 
            AND p.idBrend IN('$brend')
             ";
        }
        if($pol!=0){
            $query .= " 
            AND p.idPol IN('$pol')
             ";
        }
        if($godina!=0){
            $query .= "
            AND p.idGodinaIzdanja IN('$godina')
            ";
        }
        if($search!=""){
            $query .= "
            AND p.Naziv LIKE '%" . $search . "%'
            ";
        }
        return \DB::select($query);
       
    }

    public function getAll(){
        return \DB::table("patike as p")
            ->join("cena as c","p.idCena","=","c.idCena")
            ->join("slika as s","p.idSlika","=","s.idSlika")
            ->join("pol","p.idPol","=","pol.idPol")
            ->join("brend as b","p.idBrend","=","b.idBrend")
            ->join("godinaizdanja as gi","p.idGodinaIzdanja","=","gi.idGodinaIzdanja")
            ->select("p.idPatika","p.Naziv","b.Naziv as brend","c.originalnaCena","c.popust","s.Putanja","s.NazivSlike","pol.Naziv as poll","gi.godinaIzdanja")
            ->orderBy('p.idPatika', 'desc')
            ->paginate(5);
    }

    public function insertPatika($brend,$naziv,$pol,$godina,$idCena,$idImage){
        return \DB::table("patike")
            ->insertOrIgnore(
                ['idBrend' => $brend, 'Naziv'=>$naziv,'idPol'=>$pol,'idGodinaIzdanja'=>$godina,'idSlika'=>$idImage,'idCena'=>$idCena]
            );
    }
    public function updatePatika($brend,$naziv,$pol,$godina,$id){
        return \DB::table("patike")->where(
            ['idPatika' => $id]
        )
            ->update(
                ['idBrend' => $brend, 'Naziv'=>$naziv,'idPol'=>$pol,'idGodinaIzdanja'=>$godina]
            );
    }
    public function deletePatika($id){
        return \DB::table("patike")->where(
            ['idPatika' => $id]
        )->delete();
    }
}
