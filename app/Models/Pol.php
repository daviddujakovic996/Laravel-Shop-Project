<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pol extends Model
{
    public function getAll(){
        return \DB::table("pol")->select("*")->get();
    }
}
