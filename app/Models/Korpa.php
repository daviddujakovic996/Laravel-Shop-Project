<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Korpa extends Model
{
    public function InsertIntoCart($korisnik, $patika,$kolicina,$ukupnaCena){
        return \DB::table("korpa")
            ->insertOrIgnore(
                ['IdKorisnik' => $korisnik, 'IdPatika' => $patika, 'Kolicina'=>$kolicina, "UkupnaCena"=>$ukupnaCena]
                );
    }
    public function duplicateRows($korisnik, $patika){
        return \DB::table("korpa")
        ->select("*")
        ->where([
            ["IdKorisnik", "=", $korisnik],
            ["IdPatika", "=", $patika],
            ["Narucen", "=", 0]
        ])
        ->first(); 
    }

    public function getAll($korisnik){
        return \DB::table("korpa as k")
        ->select("k.IdKorpa","k.IdPatika","k.Kolicina","k.UkupnaCena","p.Naziv","s.Putanja")
        ->join("patike as p","k.IdPatika","=","p.idPatika")
        ->join("slika as s","p.idSlika","=","s.idSlika")
        ->where([
            ["k.IdKorisnik", "=", $korisnik],
            ["k.Narucen", "=", 0]
        ])
        ->get(); 
    }

    public function deleteFromCartProduct($idkorpa){
        return \DB::table('korpa')->where('IdKorpa', '=', $idkorpa)->delete();
    }

    public function updateStatusKorpa($korisnik){
        return \DB::table('korpa')
            ->where('IdKorisnik', (int)$korisnik)
            ->update(['Narucen' => 1]);
    }

    public function deleteUser($korisnik){
        return \DB::table('korpa')->where('IdKorisnik', '=', $korisnik)->delete();
    }

    public function dohvatiSve(){
        return \DB::table("korpa as k")
        ->select("k.IdKorpa","p.IdPatika","k.Kolicina","k.UkupnaCena","p.Naziv","s.Putanja","kor.ImePrezime","k.Narucen")
        ->join("korisnici as kor","k.IdKorisnik","=","kor.idKorisnik")
        ->join("patike as p","k.IdPatika","=","p.idPatika")
        ->join("slika as s","p.idSlika","=","s.idSlika")
        ->whereIn("k.Narucen",[1,2,3])
        ->paginate(4); 
    }

    public function prihvati($id){
        return \DB::table('korpa')
            ->where('IdKorpa', (int)$id)
            ->update(['Narucen' => 2]);
    }
    public function odbij($id){
        return \DB::table('korpa')
            ->where('IdKorpa', (int)$id)
            ->update(['Narucen' => 3]);
    }

    public function getAccept($korisnik){
        return \DB::table("korpa as k")
        ->select("k.IdKorpa","k.IdPatika","k.Kolicina","k.UkupnaCena","p.Naziv","s.Putanja")
        ->join("patike as p","k.IdPatika","=","p.idPatika")
        ->join("slika as s","p.idSlika","=","s.idSlika")
        ->where([
            ["k.IdKorisnik", "=", $korisnik],
            ["k.Narucen", "=", 2]
        ])
        ->paginate(3); 
    }
}
