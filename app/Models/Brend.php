<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brend extends Model
{
    public function getAll(){
        return \DB::table("brend")->select("*")->get();
    }
    public function insertBrend($naziv){
        return \DB::table("brend")->insert(
            ['Naziv' => $naziv]
        );
    }
    public function deleteBrend($id){
        return \DB::table("brend")->where(
            ['idBrend' => $id]
        )->delete();
    }
    public function getOne($id){
        return \DB::table("brend")->select("*")->where(
            ['idBrend' => $id]
        )->get();
    }
    public function updateBrend($naziv,$id){
        return \DB::table("brend")->where(
            ['idBrend' => $id]
        )
            ->update(
             ['Naziv'=>$naziv]
            );
    }
}
