<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GodinaIzdanja extends Model
{
    public function getAll(){
        return \DB::table("godinaizdanja")->select("*")->get();
    }
    public function deleteGodina($id){
        return \DB::table("godinaizdanja")->where(
            ['idGodinaIzdanja' => $id]
        )->delete();
    }
    public function isertGodina($naziv){
        return \DB::table("godinaizdanja")->insert(
            ['godinaIzdanja' => $naziv]
        );
    }
}
