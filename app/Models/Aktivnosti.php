<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aktivnosti extends Model
{
    public function insert($putanja="",$korisnik=""){
        return \DB::table("aktivnostikorisnika")->insert(
            ['putanja' => $putanja,'korisnik'=> $korisnik]
          );
    }
    public function getAll(){
        return \db::table("aktivnostikorisnika")->select("*")->paginate(15);
    }
}
