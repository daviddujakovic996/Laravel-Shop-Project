<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Korisnik extends Model
{
    public function insertUser($ImePrezime,$email,$lozinka){
        return \DB::table("korisnici")
            ->insertOrIgnore(
            ['ImePrezime' => $ImePrezime, 'email' => $email, 'lozinka'=>$lozinka]
            );
    }
    public function getUser($email){
        return \DB::table("korisnici")
        ->select("email")->where(["email","=",$email])->get();
    }
    public function getId($email){
        return \DB::table("korisnici")
        ->select("idKorisnik")->where("Email","=",$email)->first();
    }
    public function getByEmailAndPassword($email,$lozinka){
        return \DB::table("korisnici AS k")
        ->join("uloga AS u", "k.idUloga", "=", "u.idUloga")
        ->select("k.*", "u.NazivUloge")
        ->where([
            ["Email", "=", $email],
            ["Lozinka", "=", $lozinka]
        ])
        ->first(); 
    }
    public function updatePassword($korisnik,$staralozinka,$novalozinka){
            return \DB::table("korisnici")
            ->where("idKorisnik",(int)$korisnik)->where('Lozinka',$staralozinka)
            ->update(["Lozinka"=>$novalozinka]);
    }
    public function getAll(){
        return \DB::table("korisnici AS k")
        ->join("uloga AS u", "k.idUloga", "=", "u.idUloga")
        ->select("k.*", "u.NazivUloge")
        ->paginate(6);
        
    }
    public function deleteUser($korisnik){
        return \DB::table('korisnici')->where('idKorisnik', '=', $korisnik)->delete();
    }
}
