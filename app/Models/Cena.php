<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cena extends Model
{
    public function insertCena($cena,$popust){
        $id= \DB::table("cena")
            ->insertGetId(
                ['originalnaCena' => $cena, 'popust'=>$popust]
            );

        return $id;
    }
    public function updateCena($cena,$popust,$id){
        return \DB::table("cena")
        ->where(
            ['idCena' => $id]
        )
            ->update(
                ['originalnaCena' => $cena, 'popust'=>$popust]
            );

       
    }
}
