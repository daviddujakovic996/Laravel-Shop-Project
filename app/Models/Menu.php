<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function getAll(){
    return \DB::table("menu")->orderBy('idMenu', 'asc')->get();
    }
}
