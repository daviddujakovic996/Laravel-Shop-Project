<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PagesController@home");
Route::redirect('/Početna', '/sajtphp2/public/');

Route::get("/registracija","PagesController@register")->middleware(["isLoggedIn"]);
Route::post("/registracija","AuthController@register");

Route::get("/login","PagesController@login")->middleware(["isLoggedIn"]);
Route::post("/login","AuthController@login");

Route::get("/logout","AuthController@logout");

Route::get("/Kontakt","PagesController@kontakt")->middleware(["issAdmin"]);
Route::post("/Kontakt","KontaktController@kontaktAdmin");

Route::get("/Moj Nalog","PagesController@mojNalog")->middleware(["isNotLoggedIn"]);
Route::post("/Moj Nalog","PorukeController@updateStatus");
Route::post("/promenaLozinke","AuthController@passwordChange");


Route::get("/patika/{id?}", "PagesController@patika")->middleware(["issAdmin"])
    ->where(["id"=> "\d+"]);
Route::post("/dodajUKorpu", "KorpaController@dodajPatikuUKorpu");

Route::get("/korpa","PagesController@korpa")->middleware(["isNotLoggedIn"]);
Route::post("/obrisiKorpa","KorpaController@obrisiIzKorpeProizvod");
Route::post("/naruci","KorpaController@naruciProizvode");

Route::get("/Shop","PagesController@shop")->middleware(["issAdmin"]);
Route::post("/Shop","PatikeController@filterData");

Route::prefix("/adminPanel")->middleware(["admin"])->group(function(){
    
    Route::get("/","Admin\PagesController@home");
    Route::get("/mojNalog","Admin\PagesController@mojNalog");
    Route::post("/obrisiKorisnika","Admin\KorisnikController@deleteUser");
    Route::post("/promenaLozinke","Admin\AuthController@passwordChange");
    Route::get("/patike","Admin\PagesController@patike");
    Route::get("/patike/dodaj","Admin\PagesController@dodajProizvod");
    Route::post("/patike/dodaj","Admin\PatikaController@insert");
    Route::get("/patike/izmeni/{id?}", "Admin\PagesController@izmeniProizvod")
    ->where(["id"=> "\d+"]);
    Route::post("/patike/izmeni","Admin\PatikaController@update");
    Route::post("/patike/obrisi/{id?}", "Admin\PatikaController@delete")
    ->where(["id"=> "\d+"]);
    Route::get("/godinaIzdanja","Admin\PagesController@godina");
    Route::get("/godinaIzdanja/{id?}","Admin\PatikaController@obrisiGodina")->where(["id"=> "\d+"]);
    Route::post("/godinaIzdanja","Admin\PatikaController@unesiGodinu");

    Route::get("/brend","Admin\PagesController@brend");
    Route::post("/brend","Admin\PatikaController@unesiBrend");
    Route::get("/brend/{id?}","Admin\PatikaController@obrisiBrend")->where(["id"=> "\d+"]);
    Route::get("/brend/izmeni/{id?}","Admin\PagesController@izmeniBrend")->where(["id"=> "\d+"]);
    Route::post("/brend/izmeni","Admin\PatikaController@izmeniBrend");

    Route::get("/korpa","Admin\PagesController@korpa");
    Route::post("/korpa/prihvati","Admin\KorpaController@accept");
    Route::post("/korpa/odbij","Admin\KorpaController@denied");

    Route::get("/poruke","Admin\PagesController@poruke");
    Route::get("/poruke/odgovori/{email?}/{id?}","Admin\PagesController@odgovori");
    Route::post("/poruke/odgovori","Admin\PorukeController@odgovori");

    Route::get("/aktivnosti","Admin\PagesController@aktivnost");
});


