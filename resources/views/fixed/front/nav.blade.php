</body>
<div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="{{url("/home") }}" method="post">
                        @csrf
                            <input type="search"  class="search">
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 <!-- ##### Main Content Wrapper Start ##### -->
 <div class="main-content-wrapper d-flex clearfix">

<!-- Mobile Nav (max width 767px)-->
<div class="mobile-nav">
    <!-- Navbar Brand -->
    <div class="amado-navbar-brand">
        <a href="{{url("/") }}"><img src="{{asset("img/core-img/logo.png") }}" alt=""></a>
    </div>
    <!-- Navbar Toggler -->
    <div class="amado-navbar-toggler">
        <span></span><span></span><span></span>
    </div>
</div>

<!-- Header Area Start -->
<header class="header-area clearfix">
    <!-- Close Icon -->
    <div class="nav-close">
        <i class="fa fa-close" aria-hidden="true"></i>
    </div>
    <!-- Logo -->
    <div class="logo">
        <a href="{{url("/") }}"><img src="{{asset("img/core-img/logo.png") }}" alt=""></a>
    </div>
    <!-- Amado Nav -->
    <nav class="amado-nav">
        <ul>
        @foreach($menu as $link)
        @if($link->Naziv=="Moj Nalog")
          @if(session()->has("korisnik"))
          <li><a href="{{url("/$link->Naziv") }}">{{$link->Naziv }}</a></li>   
          @endif
         @break
        @endif
      <li><a href="{{url("/$link->Naziv") }}">{{$link->Naziv }}</a></li>   
       @endforeach
            
        </ul>
    </nav>
    <!-- Button Group -->
    <div class="amado-btn-group mt-30 mb-100">
    @if(!session()->has("korisnik"))
        <a href="{{url("/login") }}" class="btn amado-btn mb-15">Login</a>
        <a href="{{url("/registracija") }}" class="btn amado-btn active">Registracija</a>
        @else
        <a href="{{url("/logout") }}" class="btn amado-btn mb-15">Logout</a>
        @endif
    </div>
    <!-- Cart Menu -->
    <div class="cart-fav-search mb-100">
    @if(session()->has("korisnik"))
        <a href="{{url("/korpa") }}" class="cart-nav"><img src="{{asset("img/core-img/cart.png") }}" alt=""> Korpa </a>
    @endif
    @yield("pretraga")
        
    </div>
    <!-- Social Button -->
    <div class="social-info d-flex justify-content-between">
        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    </div>
</header>
<!-- Header Area End -->