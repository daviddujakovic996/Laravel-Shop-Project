<!-- ##### Footer Area Start ##### -->
<footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                        <a href="{{url("/") }}"><img src="{{asset("img/core-img/logo2.png") }}" alt=""></a>
                        </div>
                        <!-- Copywrite Text -->
                        <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#footerNavContent" aria-controls="footerNavContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                    @foreach($menu as $link)
                                        @if($link->Naziv=="Moj Nalog")
                                        @if(session()->has("korisnik"))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url("/$link->Naziv") }}">{{$link->Naziv }}</a>
                                        </li>
                                      
                                        @endif
                                        @break
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url("/$link->Naziv") }}">{{$link->Naziv }}</a>
                                        </li>
                                    @endforeach
                                    <li class="nav-item"><a class="nav-link" href="https://www.linkedin.com/in/david-dujakovi%C4%87-2759671a2/">Autor</a></li>
	                              <li class="nav-item"><a class="nav-link" href="Dokumentacija.pdf">Dokumentacija</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="{{asset("js/jquery/jquery-2.2.4.min.js") }}"></script>
    <!-- Popper js -->
    <script src="{{asset("js/popper.min.js") }}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset("js/bootstrap.min.js") }}"></script>
    <!-- Plugins js -->
    <script src="{{asset("js/plugins.js") }}"></script>
    <!-- Active js -->
    <script src="{{asset("js/active.js") }}"></script>



    
</body>

</html>