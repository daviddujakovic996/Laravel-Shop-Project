
 <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                          
                            <div class="header-button">
                                <div class="noti-wrap">
                                    
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity"></span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>Poruke</p>
                                            </div>
                                            @foreach($poruke as $p)
                                            @if($p->status==0)
                                            <div class="email__item">
                                                <div class="content">
                                                    <a href="{{url("/adminPanel/poruke") }}">
                                                    <p>{{$p->FromEmail}}</p>
                                                    <span>{{$p->Naslov}}</span>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                           
                                        </div>
                                    </div>
                                  
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <img src="{{ asset("img/bg-img/autor.jpg") }}" alt="John Doe" />
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">
                                            @if(session()->has('korisnik'))
                          
                                             {{ session('korisnik')->ImePrezime}}
                                          
                                         
                                            @endif
                                               
                                            </a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <img src="{{ asset("img/bg-img/autor.jpg") }}" alt="John Doe" />
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{ session('korisnik')->ImePrezime}}</a>
                                                    </h5>
                                                    <span class="email">{{ session('korisnik')->Email}}</span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="{{url("/adminPanel/mojNalog") }}">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="{{url("/logout") }}">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->
            