
<body class="animsition">
    <div class="page-wrapper">
     

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="{{url("/adminPanel") }}">
                    <img src="{{ asset("admin/images/icon/logo.png") }}" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="{{url("adminPanel/mojNalog") }}">
                                <i class="fas fa-chart-bar"></i>Moj Nalog</a>
                        </li>
                        <li>
                        <a href="{{url("/adminPanel/poruke") }}">
                        <i class="fas fa-chart-bar"></i>Poruke</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Tabele</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="{{url("adminPanel/patike") }}">Patike</a>
                                </li>
                                <li>
                                    <a href="{{url("adminPanel/godinaIzdanja") }}">Godina izdanja</a>
                                </li>
                                <li>
                                    <a href="{{url("adminPanel/brend") }}">Brend</a>
                                </li>
                                <li>
                                    <a href="{{url("adminPanel/korpa") }}">Korpa</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{url("adminPanel/aktivnosti") }}">
                                <i class="fas fa-chart-bar"></i>Aktivnosti</a>
                        </li>
                        
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->