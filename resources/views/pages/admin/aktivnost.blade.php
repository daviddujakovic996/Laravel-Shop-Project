@extends("layouts/admin")

@section("centralniSadrzaj")
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
<div class="col-lg-10">
                                <div class="au-card au-card--bg-blue au-card-top-countries m-b-30">
                                    <div class="au-card-inner">
                                        <div class="table-responsive">
                                            <h1>Aktivnost</h1>
                                            <table class="table table-top-countries">
                                                <tbody>
                                                <tr>
                                                        <td>Korisnik</td>
                                                        <td class="text-right">Putanja</td>
                                                        <td class="text-right">Vreme</td>
                                                    </tr>
                                                    @foreach($aktivnost as $a)
                                                    <tr>
                                                        <td>{{$a->korisnik}}</td>
                                                        <td class="text-right" style="color:aqua;">{{$a->putanja}}</td>
                                                        <td class="text-right" style="color:aqua;">{{$a->vreme}}</td>
                                                    </tr>
                                                    @endforeach
                                                   
                                                </tbody>
                                            </table>
                                            
                                            
                                        </div>
                                       
                                    </div>
                                </div> {{$aktivnost->links()}}
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                                
                            </div>

</div>
@endsection