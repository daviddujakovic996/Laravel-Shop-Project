@extends("layouts/admin")

@section("centralniSadrzaj")
 <!-- MAIN CONTENT-->
 <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Dodaj proizvod</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="{{url("/adminPanel/patike/dodaj")}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        @csrf
                                          
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label" name="naziv">Naziv proizvoda</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" name="naziv" placeholder="Naziv" class="form-control">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="email-input" class=" form-control-label">Cena</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="email-input" name="cena" placeholder="Cena" class="form-control" step=".01">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="password-input" class=" form-control-label">Popust</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="password-input" name="popust" placeholder="Popust" class="form-control" step=".01">
                                                
                                                </div>
                                            </div>    
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Brend</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="brend" id="select" class="form-control">
                                                        @foreach($brend as $b)
                                                        <option value="{{$b->idBrend}}">{{$b->Naziv}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Pol</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="pol" id="select" class="form-control">
                                                        @foreach($pol as $p)
                                                        <option value="{{$p->idPol}}">{{$p->Naziv}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Godina izdanja</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="godinaIzdanja" id="select" class="form-control">
                                                        @foreach($godina as $g)
                                                        <option value="{{$g->idGodinaIzdanja}}">{{$g->godinaIzdanja}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="file-input" class=" form-control-label">Unos slike</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="file" id="file-input" name="slika" class="form-control-file">
                                                </div>
                                            </div>
                                            
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Unesi
                                        </button>
                                        
                                    </div>
                                </div>
                                
                            </div>

</div>
@endsection