@extends("layouts/admin")

@section("centralniSadrzaj")
  <!-- MAIN CONTENT-->
  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                        <h1>Korisnici</p>     
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Ime i Prezime</th>
                                                <th>Email</th>
                                                <th>Uloga</th>
                                                <th>obrisi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($korisnici as $k)
                                            <tr>
                                                <td>{{$k->ImePrezime}}</td>
                                                <td>{{$k->Email}}</td>
                                                <td>{{$k->NazivUloge}}</td>
                                                <td>
                                                <form action="{{url("/adminPanel/obrisiKorisnika") }}" method="post">
                                                @csrf
                                                <input type="submit" class="btn btn-primary btn-sm" value="Obrisi"/>
                                                <input type="hidden" name="obrisi" value="{{$k->idKorisnik}}"/>
                                                </form>
                                                
                                                </td>
                                            </tr>
                                            @endforeach
                                            @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                                        </tbody>
                                        
                                    </table>
                                    {{ $korisnici->links() }}
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endsection