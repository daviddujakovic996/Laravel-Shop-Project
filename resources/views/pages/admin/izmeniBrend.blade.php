@extends("layouts/admin")

@section("centralniSadrzaj")
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
<div class="col-lg-10">
                                
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Izmeni brend</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="{{url("/adminPanel/brend/izmeni") }}" method="post" class="">
                                            @csrf
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Brend</label>
                                                @foreach($brend as $b)
                                                <input type="text" id="nf-email" name="brend" class="form-control" value="{{$b->Naziv}}">
                                                <input type="hidden" id="nf-email" name="idbrend" class="form-control" value="{{$b->idBrend}}">
                                                <span class="help-block">Molimo izmenite brend</span>
                                                @endforeach
                                            </div>
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Unesi
                                        </button>

                                    </div>
                                    </form>
                                </div>
                            </div>

</div>
@endsection