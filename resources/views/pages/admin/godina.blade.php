@extends("layouts/admin")

@section("centralniSadrzaj")
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
<div class="col-lg-3">
                                <div class="au-card au-card--bg-blue au-card-top-countries m-b-30">
                                    <div class="au-card-inner">
                                        <div class="table-responsive">
                                            <h1>Godina izdanja</h1>
                                            <table class="table table-top-countries">
                                                <tbody>
                                                <tr>
                                                        <td>Godina</td>
                                                        <td class="text-right">Akcija</td>
                                                    </tr>
                                                    @foreach($godina as $g)
                                                    <tr>
                                                        <td>{{$g->godinaIzdanja}}</td>
                                                        <td class="text-right"><a style="color:aqua;" href="{{url("/adminPanel/godinaIzdanja/$g->idGodinaIzdanja") }}">Obrisi</a></td>
                                                    </tr>
                                                    @endforeach
                                                   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Unesi godinu</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="{{url("/adminPanel/godinaIzdanja") }}" method="post" class="">
                                            @csrf
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Godina</label>
                                                <input type="number" id="nf-email" name="godina" placeholder="Unesi godinu.." class="form-control">
                                                <span class="help-block">Molimo unesite godinu</span>
                                            </div>
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Unesi
                                        </button>

                                    </div>
                                    </form>
                                </div>
                            </div>

</div>
@endsection