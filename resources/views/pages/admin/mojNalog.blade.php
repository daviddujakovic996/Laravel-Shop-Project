@extends("layouts/admin")
@section("centralniSadrzaj")
<!-- MAIN CONTENT-->
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                        <h1>Admin</p>     
                        <div class="row m-t-30">
                            <div class="col-md-12">
@if(session()->has('korisnik'))
                          
                            <h1>{{ session('korisnik')->ImePrezime}}</h1>
                            <h2>{{ session('korisnik')->Email}}</h2>
                            <br/></br>
                             @endif
                             <div class="flip">Promena lozinke</i></div>
                            <div class="panel">
                            @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset

                            
                            <form action="{{ url("/adminPanel/promenaLozinke") }}" method="POST">
                            @csrf
                                <div class="row">                              
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Stara lozinka"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Nova Lozinka"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="renewpassword" id="renewpassword" placeholder="Ponovite novu Lozinka"/>
                                    </div>
                                    

                                </div>
                                <div class="cart-btn mt-100">
                                <input type="submit" class="btn amado-btn w-100" value="Promeni lozinku"/>
                                </div>
                            </form>
                            </div>@if(session()->has('message'))
                           
                           {{ session('message') }}
                           @endif
                        </div>
                    </div>
                    
                </div>
         
    </div>
    <script src="{{ asset("/js/jquery/jquery-2.2.4.min.js")}}"></script>
    <script>
    $(document).ready(function(){
  $(".flip:first").click(function(){
    $(".panel:first").slideToggle("slow");
  });



  
  

  
});  

    </script>
    <style> 
.panel, .flip {
  padding: 5px;
  text-align: center;
  background-color: #e5eecc;
  border: solid 1px #c3c3c3;
  max-width:none;
}

.panel {
  padding: 50px;
  display: none;
  
}
.prikaz{
    padding: 50px;
  display: none;
  padding: 5px;
  text-align: left;
  background-color: #e5eecc;
  border: solid 1px #c3c3c3;
  min-width:600px;
}
</style>
    @endsection