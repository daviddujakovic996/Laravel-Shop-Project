@extends("layouts/admin")

@section("centralniSadrzaj")
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
<div class="col-lg-8">
                               
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Unesi godinu</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="{{url("/adminPanel/poruke/odgovori") }}" method="post" class="">
                                            @csrf
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Email</label>
                                                <input type="text" id="nf-email" name="email"  class="form-control" value="{{$email}}" disabled><br/>
                                                <input type="hidden" id="nf-email" name="emailskr"  class="form-control" value="{{$email}}">
                                                <input type="hidden" id="nf-email" name="idskr"  class="form-control"value="{{$idPorukee}}">
                                                <input type="text" id="nf-email" name="naslov"  class="form-control" placeholder="Unesi naslov"><br/>
                                                <input type="text" id="nf-email" name="tekst"  class="form-control" placeholder="Unesi poruku"><br/>
                                            </div>
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Odgovori
                                        </button>

                                    </div>
                                    </form>
                                </div>
                            </div>

</div>
@endsection