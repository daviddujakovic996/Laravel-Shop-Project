@extends("layouts/admin")

@section("centralniSadrzaj")


              <!-- MAIN CONTENT-->
  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                        <h1>Korpa</p>     
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                          
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Slika</th>
                                                <th>Naziv</th>
                                                <th>Korisnik</th>
                                                <th>Kolicina</th>
                                                <th>Ukupna cena</th>
                                                <th>Status</th>
                                                <th>Prihvati</th>
                                                <th>Odbij</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($korpa as $k)
                                            <tr>
                                                <td><img src="{{ asset("img/bg-img/$k->Putanja") }}"/></td>
                                                <td>{{$k->Naziv}}</td>
                                                <td>{{$k->ImePrezime}}</td>
                                                <td>{{$k->Kolicina}}</td>
                                                <td>{{$k->UkupnaCena}}</td>
                                                @if($k->Narucen==1)
                                                <td class="process">Narucen</td>
                                                <form action="{{url("/adminPanel/korpa/prihvati") }}" method="post">
                                                @csrf
                                                <td><input type="submit" value="Prihvati" class="btn btn-primary btn-sm"/></td>
                                                <input type="hidden" name="korpa" value="{{$k->IdKorpa}}"/>
                                                </form>
                                                <form action="{{url("/adminPanel/korpa/odbij") }}" method="post">
                                                @csrf
                                                <td><input type="submit" value="Odbij" class="btn btn-danger btn-sm"/></td>
                                                <input type="hidden" name="korpa" value="{{$k->IdKorpa}}"/>
                                                </form>
                                                @elseif($k->Narucen==2)
                                                <td style="color:aqua;">Prihvacen</td>
                                                <td></td>
                                                <td></td>
                                                @else
                                                <td class="denied">Odbijen</td>
                                                <td></td>
                                                <td></td>

                                                @endif
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                       
                                    </table>
                                   
                                </div>
                                <!-- END DATA TABLE-->
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    {{ $korpa->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection