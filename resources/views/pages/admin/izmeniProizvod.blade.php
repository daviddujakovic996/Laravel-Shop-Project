@extends("layouts/admin")

@section("centralniSadrzaj")
 <!-- MAIN CONTENT-->
 <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Izmeni proizvod</strong> 
                                    </div>
                                    <div class="card-body card-block">
                     
                                        <form action="{{url("/adminPanel/patike/izmeni")}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        @csrf
                                            @foreach($patika as $p)
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label" name="naziv">Naziv proizvoda</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" name="naziv" placeholder="Naziv" class="form-control" value="{{$p->Naziv}}">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="email-input" class=" form-control-label">Cena</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="email-input" name="cena" placeholder="Cena" class="form-control" step=".01" value="{{$p->originalnaCena}}">
                                                    
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="password-input" class=" form-control-label">Popust</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="number" id="password-input" name="popust" placeholder="Popust" class="form-control" step=".01" value="{{$p->popust}}">
                                                
                                                </div>
                                            </div>    
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Brend</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="brend" id="select" class="form-control">
                                                    <option value="{{$p->idBrend}}">{{$p->brend}}</option>
                                                        @foreach($brend as $b)
                                                        <option value="{{$b->idBrend}}">{{$b->Naziv}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Pol</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="pol" id="select" class="form-control">
                                                        <option value="{{$p->idPol}}">{{$p->poll}}</option>
                                                        @foreach($pol as $pp)
                                                        <option value="{{$pp->idPol}}">{{$pp->Naziv}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Godina izdanja</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="godinaIzdanja" id="select" class="form-control">
                                                        <option value="{{$p->idGodinaIzdanja}}">{{$p->godinaIzdanja}}</option>
                                                        @foreach($godina as $g)
                                                        <option value="{{$g->idGodinaIzdanja}}">{{$g->godinaIzdanja}}</option>
                                                        @endforeach
  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="file-input" class=" form-control-label">Unos slike</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="file" id="file-input" name="image" class="form-control-file">
                                                    <input type="hidden" name="defaultSlika" value="{{$p->Putanja}}">
                                                    <input type="hidden" name="idCena" value="{{$p->idCena}}">
                                                    <input type="hidden" name="idSlika" value="{{$p->idSlika}}">
                                                    <input type="hidden" name="idPatika" value="{{$p->idPatika}}">
                                                </div>
                                               
                                            </div>
                                            <div class="card-footer">
                                        <input type="submit" class="btn btn-primary btn-sm" value="izmeni"/>
                                           
                                        
                                    </div>
                                    @endforeach
                                        </form>
                                    </div>
                                    
                                </div>
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           
  @isset($errors)
      @foreach($errors->all() as $error)
              {{ $error }}
      @endforeach
  @endisset
                                
                            </div>

</div>
@endsection