@extends("layouts/admin")

@section("centralniSadrzaj")
  <!-- MAIN CONTENT-->
  <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                        <h1>Proizvodi</p>     
                        <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                <a href="{{url("/adminPanel/patike/dodaj") }}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>add item</a>
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>Slika</th>
                                                <th>Naziv</th>
                                                <th>Brend</th>
                                                <th>Originalna Cena</th>
                                                <th>Popust</th>
                                                <th>Pol</th>
                                                <th>Godina Izdanja</th>
                                                <th>Izmeni</th>
                                                <th>obrisi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            @foreach($patike as $p)
                                            <tr>
                                                <td><img src="{{ asset("img/bg-img/$p->Putanja") }}" alt="{{$p->NazivSlike}}"></td>
                                                <td>{{$p->Naziv}}</td>
                                                <td>{{$p->brend}}</td>
                                                <td>{{$p->originalnaCena}}</td>
                                                <td>{{$p->popust}}</td>
                                                <td>{{$p->poll}}</td>
                                                <td>{{$p->godinaIzdanja}}</td>
                                                <td>           
                                                <a href="{{url("/adminPanel/patike/izmeni/$p->idPatika") }}" class="btn btn-primary btn-sm">Izmeni</a>                
                                                </td>
                                                <td>
                                                <form action="{{url("/adminPanel/patike/obrisi/$p->idPatika") }}" method="post">
                                                @csrf
                                                <input type="submit" class="btn btn-primary btn-sm" value="Obrisi"/>
                                                <input type="hidden" name="obrisi" value="{{$p->idPatika}}"/>
                                                </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                                        </tbody>
                                        
                                    </table>
                                    {{ $patike->links() }}
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endsection