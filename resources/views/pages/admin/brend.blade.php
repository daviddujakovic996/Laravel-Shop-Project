@extends("layouts/admin")

@section("centralniSadrzaj")
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
<div class="col-lg-10">
                                <div class="au-card au-card--bg-blue au-card-top-countries m-b-30">
                                    <div class="au-card-inner">
                                        <div class="table-responsive">
                                            <h1>Brend</h1>
                                            <table class="table table-top-countries">
                                                <tbody>
                                                <tr>
                                                        <td>Brend</td>
                                                        <td class="text-right">Akcija</td>
                                                        <td class="text-right">Akcija</td>
                                                    </tr>
                                                    @foreach($brend as $b)
                                                    <tr>
                                                        <td>{{$b->Naziv}}</td>
                                                        <td class="text-right"><a style="color:aqua;" href="{{url("/adminPanel/brend/$b->idBrend") }}">Obrisi</a></td>
                                                        <td class="text-right"><a style="color:aqua;" href="{{url("/adminPanel/brend/izmeni/$b->idBrend") }}">Izmeni</a></td>
                                                    </tr>
                                                    @endforeach
                                                   
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Unesi brend</strong> 
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="{{url("/adminPanel/brend") }}" method="post" class="">
                                            @csrf
                                            <div class="form-group">
                                                <label for="nf-email" class=" form-control-label">Brend</label>
                                                <input type="text" id="nf-email" name="brend" placeholder="Unesi brend.." class="form-control">
                                                <span class="help-block">Molimo unesite brend</span>
                                            </div>
                                            
                                        
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Unesi
                                        </button>

                                    </div>
                                    </form>
                                </div>
                            </div>

</div>
@endsection