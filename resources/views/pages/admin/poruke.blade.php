@extends("layouts/admin")
@section("centralniSadrzaj")
<!-- MAIN CONTENT-->
<div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        
                          <h1>Neprocitane poruke</h1>
                        <div class="row m-t-30">
                            <div class="col-md-12">
                            @if(count($poruke)>0)
                            
                            @foreach($poruke as $p)
                            
                            <div class="panel">
                            @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset
                            
                            <input type="hidden" id="skriveno" value="{{$p->Naslov}}"/>
                          @csrf
                            @if($p->status==0)
                            <p class="procitano">Neprocitano</p>
                            @else
                            <p>Procitano</p>
                            @endif
                            
                            <p style="margin-right:auto;">Naslov:{{$p->Naslov}}</p>
                            
                            <p>Od:{{$p->FromEmail}}</p>
                            <p class="prikaz">Poruka: {{$p->Poruka}}</p><br/>
                            <a href="{{url("/adminPanel/poruke/odgovori/$p->FromEmail/$p->idPoruka") }}">Odgovori</a>
                            <hr/>
                            
                           
                            </div>
                            
                            
                            @endforeach
                            
                            @else
                            <p>NEMATE NOVIH PORUKA</p>
                            @endif
                             

                            
                            
                            </div>
                            @if(session()->has('message'))
                           
                           {{ session('message') }}
                           @endif
                        </div>
                    </div>
                    
                </div>
         
    </div>
  
    @endsection