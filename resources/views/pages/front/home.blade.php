@extends("layouts/front")

@section("centralniSadrzaj")
@if(session()->has("korisnik"))
            @if(session("korisnik")->NazivUloge == "Admin")
                <script>window.location += "adminPanel/";</script>
          @endif
  @endif
<!-- Product Catagories Area Start -->
<div class="products-catagories-area clearfix">
@if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           
  @isset($errors)
      @foreach($errors->all() as $error)
              {{ $error }}
      @endforeach
  @endisset
    <br/><br/><h2 style="text-align:center">Patike na akciji</h2><hr>
            <div class="amado-pro-catagory clearfix">
                @foreach($patike as $patika)
                <div class="single-products-catagory clearfix">
                    <a href="{{url("/patika/$patika->idPatika") }}">
                    <img src="{{ asset("img/bg-img/$patika->Putanja") }}" alt="{{$patika->NazivSlike}}">
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <div class="line"></div>
                            <p>{{$patika->originalnaCena-$patika->popust}} din</p>
                            <del>{{$patika->originalnaCena}} din</del>
                            <h4>{{$patika->Naziv}}</h4>
                        </div>
                    </a>
                </div>
                   
                @endforeach
               
                
            </div>
        </div>
        <!-- Product Catagories Area End -->
    </div>
    <!-- ##### Main Content Wrapper End ##### -->


@endsection