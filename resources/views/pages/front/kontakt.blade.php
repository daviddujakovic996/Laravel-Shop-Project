@extends("layouts/front")

@section("centralniSadrzaj")
<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Kontakt</h2>
                            </div>
                            <div id="ispis">
                            @if(session()->has('message'))
                          
                            {{ session('message') }}
                             @endif
                             
    @isset($errors)
        @foreach($errors->all() as $error)
                {{ $error }}
        @endforeach
    @endisset
                            </div>
                            
                            <form action="{{ url("/Kontakt") }}" method="POST">
                            @csrf
                                <div class="row">                              
                                    <div class="col-12 mb-3">
                                    @if(session()->has('korisnik'))
                                        <input type="email" class="form-control" name="emaill" id="emaill" value="{{ session('korisnik')->Email }}" disabled/>
                                        <input type="hidden" class="form-control" name="email" id="email" value="{{ session('korisnik')->Email }}" />
                                    @else
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email"/>
                                    @endif
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control" name="naslov" id="naslov" placeholder="Naslov"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <textarea  rows="4" cols="50" class="form-control" name="tekst" id="tekst" placeholder="Tekst"></textarea>
                                    </div>

                                </div>
                                <div class="cart-btn mt-100">
                                <input type="submit" class="btn amado-btn w-100" value="Posalji poruku"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    @endsection