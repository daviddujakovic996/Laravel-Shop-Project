@extends("layouts/front")


@section("centralniSadrzaj")
<div class="single-product-area section-padding-100 clearfix">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <div class="single_product_thumb">
                            <div id="product_details_slider" class="carousel slide" data-ride="carousel">
                             
                            @foreach($patika as $p)
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <a class="gallery_img" href="{{asset("img/bg-img/$p->Putanja") }}">
                                            <img class="d-block w-100" src="{{asset("img/bg-img/$p->Putanja") }}" alt="{{$p->NazivSlike}}">
                                        </a>
                                    </div>
                            @endforeach       
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5">
                        <div class="single_product_desc">
                            <!-- Product Meta Data -->
                            @foreach($patika as $p)
                            <div class="product-meta-data">
                                <div class="line"></div>
                                <p class="product-price">
                                    @if($p->popust>0)
                                    <p>{{$p->originalnaCena-$p->popust}} din</p>
                                    <del>{{$p->originalnaCena}} din</del>
                                    @else
                                    <p>{{$p->originalnaCena}} din</p>
                                    @endif
                                </p>
                                <a href="#">
                                    <h6>{{$p->Naziv}}</h6>
                                </a>
                                <!-- Ratings & Review -->
                                
                                <!-- Avaiable -->
                                <p class="avaibility"><i class="fa fa-circle"></i> In Stock</p>
                            </div>
                            
                            
                            @if(session()->has("korisnik"))
                            <!-- Add to Cart Form -->
                            <form class="cart clearfix" action="{{ url("/dodajUKorpu") }}" method="post">
                            @csrf
                                <input type="hidden" name="patika" value="{{ $p->idPatika }}"/>
                                @if($p->popust>0)
                                <input type="hidden" name="cena" value="{{$p->originalnaCena-$p->popust}}"/>
                                @else
                                <input type="hidden" name="cena" value="{{$p->originalnaCena}}"/>
                                @endif
                                <div class="cart-btn d-flex mb-50">
                                    <p>Kolicina</p>
                                    <div class="quantity">
                                        <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                        <input type="number" class="qty-text" id="qty" step="1" min="1" max="300" name="quantity" value="1">
                                        <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-caret-up" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                               
                                <button type="submit" name="addtocart" id="addtocart" value="5" class="btn amado-btn">Dodaj u korpu</button>
                            </form>
                           
                            @else
                            <a class="btn amado-btn" href="{{ url("/registracija") }}">Registrujte se da bi dodali proizvode u korpu</a>
                            @endif
 @endforeach
 @if(session()->has('message'))
                          
                            <h2>{{ session('message') }}</h2>
                             @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Details Area End -->
    </div>
    <!-- ##### Main Content Wrapper End ##### -->

    

@endsection