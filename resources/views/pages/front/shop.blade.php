@extends("layouts/front")
@section("pretraga")
<a href="#" class="search-nav"><img src="{{asset("img/core-img/search.png") }}" alt=""> Search</a>
@endsection
@section("centralniSadrzaj")
<div class="shop_sidebar_area">

<form action="{{url("/Shop") }}" method="post">
@csrf
<!-- ##### Single Widget ##### -->
<div class="widget brands mb-50">
    <!-- Widget Title -->
    <h6 class="widget-title mb-30">Pol</h6>

    <div class="widget-desc">
        <!-- Single Form Check -->
        @foreach($pol as $p)
        <div class="form-check">
            <input class="pol form-check-input" type="checkbox" value="{{$p->idPol}}">
            <label class="form-check-label" for="amado">{{$p->Naziv}}</label>
        </div>
       @endforeach
    </div>
</div>

<!-- ##### Single Widget ##### -->
<div class="widget brands mb-50">
    <!-- Widget Title -->
    <h6 class="widget-title mb-30">Kategorije</h6>

    <div class="widget-desc">
        <!-- Single Form Check -->
        @foreach($brend as $b)
        <div class="form-check">
            <input class="brend form-check-input" type="checkbox" value="{{$b->idBrend}}">
            <label class="form-check-label" for="amado">{{$b->Naziv}}</label>
        </div>
       @endforeach
    </div>
</div>


<!-- ##### Single Widget ##### -->
<div class="widget brands mb-50">
    <!-- Widget Title -->
    <h6 class="widget-title mb-30">Godina</h6>

    <div class="widget-desc">
        <!-- Single Form Check -->
        @foreach($godina as $g)
        <div class="form-check">
            <input class="godina form-check-input" type="checkbox" value="{{$g->idGodinaIzdanja}}">
            <label class="form-check-label" for="amado">{{$g->godinaIzdanja}}</label>
        </div>
        @endforeach
    </div>
</div>



<!-- ##### Single Widget ##### -->
<div class="widget price mb-50">
    <!-- Widget Title -->
    <h6 class="widget-title mb-30">Cena</h6>

    <div class="widget-desc">
        <div class="slider-range">
            
                
                <li><label for="cena-checkbox1">Od</label><input type="number" id="hidden_minimum_price" value="0" /></li>
                <li><label for="cena-checkbox1">Do</label><input type="number" id="hidden_maximum_price" value="100000" /></li>
            
            
        </div>
    </div>
</div>
</form>
</div>

<div class="amado_product_area section-padding-100">
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="product-topbar d-xl-flex align-items-end justify-content-between">
                <!-- Total Products -->
                <div class="total-products">
                    
                    <div class="view d-flex">
                        
                    </div>
                </div>
                <!-- Sorting -->
                <div class="product-sorting d-flex">
                    <div class="sort-by-date d-flex align-items-center mr-15">
                        <p>Sort by</p>
                        <form action="{{url("/Shop") }}" method="post">
                            <select name="select" id="sortBydateAndPrice">
                                <option value="0">&nbsp; Datum:najnovije</option>
                                <option value="1">&nbsp; Datum:najstarije</option>
                                <option value="2">&nbsp; Cena:najveca</option>
                                <option value="3">&nbsp; Cena:najmanja</option>
                            </select>
                        </form>
                    </div>
                 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div id="ispis" style="display:contents;">
        <!-- Single Product Area -->
        @foreach($patike as $p)
        <div class="col-12 col-sm-6 col-md-12 col-xl-6">
            <div class="single-product-wrapper">
                <!-- Product Image -->
                <div class="product-img">
                    <img src="{{ asset("img/bg-img/$p->Putanja") }}" alt="{{$p->NazivSlike}}">
                    <img class="hover-img" src="{{ asset("img/bg-img/$p->Putanja") }}" alt="{{$p->NazivSlike}}">
                </div>

                <!-- Product Description -->
                <div class="product-description d-flex align-items-center justify-content-between">
                    <!-- Product Meta Data -->
                    <div class="product-meta-data">
                        <div class="line"></div>
                        @if($p->popust>0)
                        <p class="product-price">{{$p->originalnaCena-$p->popust}} din</p>
                        <del>{{$p->originalnaCena}} din</del>
                         @else
                         <p class="product-price">{{$p->originalnaCena}} din</p>
                         @endif
                        
                        <a href="{{url("/patika/$p->idPatika") }}">
                            <h6>{{$p->Naziv}}</h6>
                        </a>
                    </div>
                    <!-- Ratings & Cart -->
                    <div class="ratings-cart text-right">
                        
                        <div class="cart">
                            <a href="{{url("/patika/$p->idPatika") }}" data-toggle="tooltip" data-placement="left" title="Dodaj u korpu"><img src="{{asset("img/core-img/cart.png") }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        </div>
        
       
    </div>

    <div class="row">
        <div class="col-12">
            <!-- Pagination -->
            <nav aria-label="navigation">
            <form action="{{url("/") }}" method="post">
            @csrf
                <ul id="pagination" class="pagination justify-content-end mt-50" style="float:left">
                    
                </ul>
            </form>
            </nav>
        </div>
    </div>
</div>
</div>
</div>
<script src="{{ asset("/js/jquery/jquery-2.2.4.min.js")}}"></script>
<script src="{{ asset("/js/filter.js")}}"}}"></script>





@endsection