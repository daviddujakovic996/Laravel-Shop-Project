@extends("layouts/front")

@section("centralniSadrzaj")
<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Log in</h2>
                            </div>
                            <div id="ispis">
                            @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                           
  @isset($errors)
      @foreach($errors->all() as $error)
              {{ $error }}
      @endforeach
  @endisset
                            </div>
                            
                            <form action="{{ url("/login") }}" method="POST">
                            @csrf
                                <div class="row">                              
                                    <div class="col-12 mb-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Lozinka"/>
                                    </div>
                                    

                                </div>
                                <div class="cart-btn mt-100">
                                <input type="submit" class="btn amado-btn w-100" value="Uloguj se"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    @endsection