@extends("layouts/front")

@section("centralniSadrzaj")
<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>Shopping Cart</h2>
                        </div>

                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Naziv</th>
                                        <th>Ukupna Cena/Kolicina</th>
                                        
                                        <th>Obrisi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sviProizvodiUKorpi as $p)
                                    <tr>
              
                                        <td class="cart_product_img">
                                            <a href="#"><img src="{{asset("img/bg-img/$p->Putanja") }}" alt="Product"></a>
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5>{{$p->Naziv}}</h5>
                                        </td>
                                        <td class="price">
                                            <span>{{$p->UkupnaCena}} / {{$p->Kolicina}}</span>
                                        </td>
                                        
                                        <td>
                                            <div>                                            
                                                <div class="quantity">
                                                    <form action="{{ url("/obrisiKorpa") }}" method="post">
                                                    @csrf
                                                    <input type="submit" class="btn amado-btn w-100" name="obrisiIzKorpe" value="Obrisi"/>
                                                    <input type="hidden" name="proizvod" value="{{$p->IdKorpa}}"/>
                                                    
                                                    </form>    
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @if(session()->has('poruka'))
                          
                          {{ session('poruka') }}
                           @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>Korpa</h5>
                            <ul class="summary-table">
                              
                                <li><span>Dostava:</span> <span>Besplatno</span></li>
                                
                                <li><span>Ukupno:</span> <span>
                                <?php
                                $saberi=0;            
                                ?>
                                @foreach($sviProizvodiUKorpi as $p)
                                <?php
                                $saberi+=$p->UkupnaCena;                    
                                ?>
                                @endforeach
                                <?php 
                                echo $saberi;
                                ?>
                                </span></li>
                            </ul>
                            <div class="cart-btn mt-100">
                                    <form action="{{ url("/naruci") }}" method="post">
                                        @csrf
                                        <input type="submit" class="btn amado-btn w-100" name="naruci" value="Naruci"/>
                                       
                                                    
                                    </form>    
                               
                            </div>
                            @if(session()->has('message'))
                          
                          {{ session('message') }}
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection