@extends("layouts/front")

@section("centralniSadrzaj")
<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Moj Nalog</h2>
                            </div>
                            <div id="ispis">
                             
                            <div class="flip"><i class="fa fa-inbox" aria-hidden="true">Poruke</i></div>
                            <div class="panel">
                            @if(count($poruke)>0)
                            
                            @foreach($poruke as $p)
                            
                            <div class="slide">
                            <input type="hidden" id="skriveno" value="{{$p->Naslov}}"/>
                          @csrf
                            @if($p->status==0)
                            <p class="procitano">Neprocitano</p>
                            @else
                            <p>Procitano</p>
                            @endif
                            
                            <p style="margin-right:auto;">Naslov:{{$p->Naslov}}</p>
                            
                            <p>Od:{{$p->FromEmail}}</p>
                            <p class="prikaz">Poruka: {{$p->Poruka}}<br/>
                              <a href="{{url("/Kontakt") }}">Odgovori</a>
                            </p>
                            <hr/>
                            
                           
                            </div>
                            
                            
                            @endforeach
                            
                            @else
                            <p>NEMATE NOVIH PORUKA</p>
                            @endif
                            </div>
                            </div>
                            @if(session()->has('korisnik'))
                          
                            <h1>{{ session('korisnik')->ImePrezime}}</h1>
                            <h2>{{ session('korisnik')->Email}}</h2>
                            <br/></br>
                             @endif
                             <div class="flip">Promena lozinke</i></div>
                            <div class="panel">
                            @isset($errors)
                            @foreach($errors->all() as $error)
                           {{ $error }}
                             @endforeach
                             @endisset

                            
                            <form action="{{ url("/promenaLozinke") }}" method="POST">
                            @csrf
                                <div class="row">                              
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Stara lozinka"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Nova Lozinka"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="renewpassword" id="renewpassword" placeholder="Ponovite novu Lozinka"/>
                                    </div>
                                    

                                </div>
                                <div class="cart-btn mt-100">
                                <input type="submit" class="btn amado-btn w-100" value="Promeni lozinku"/>
                                </div>
                            </form>
                            </div>@if(session()->has('message'))
                           
                           {{ session('message') }}
                           @endif
                           <br/><br/>
                           <h1>Uspesno naruceni proizvodi</h1>
                           <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Slika</th>
                                        <th>Naziv</th>
                                        <th>Ukupna Cena/Kolicina</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                            @if(count($sviProizvodiUKorpi))
                            @foreach($sviProizvodiUKorpi as $p)
                                <tr>
                                      <td><img src="{{asset("img/bg-img/$p->Putanja") }}"/></td>
                                      <td>{{$p->Naziv}}</td>
                                      <td>{{$p->UkupnaCena}}/{{$p->Kolicina}}</td>
                                      
                                      <td style="color:aqua">Prihvacen</td>
                                </tr>
                            @endforeach
                            @endif
</tbody>
                            </table>
                            {{$sviProizvodiUKorpi->links()}}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset("/js/jquery/jquery-2.2.4.min.js")}}"></script>
    <script>
    $(document).ready(function(){
  $(".flip:first").click(function(){
    $(".panel:first").slideToggle("slow");
  });
  $(".flip:last").click(function(){
    $(".panel:last").slideToggle("slow");
  });
  $("div").click(function(){
    
    $(this).closest('.slide').find('.prikaz').stop().slideToggle("300"); 

   
  })

  $(".slide").click(function(){
    var naslov=$(this).closest('.slide').find("#skriveno").val();
   
   $.ajax({
               url: window.location,
               method: "POST",
               dataType: "json",
               data: {
                   _token: $("input[name='_token']").val(),
                   naslov:naslov
               },
               success: function(data){
               $(this).find(".procitano").html("");
               },
               error: function(error,xhr,status){
                   
               }
 });
  })
  
  $(".slide").hover(function(){
  $(this).css("background-color", "yellow");
  }, function(){
  $(this).css("background-color", "#e5eecc");
});
  
});  

    </script>
    <style> 
.panel, .flip {
  padding: 5px;
  text-align: center;
  background-color: #e5eecc;
  border: solid 1px #c3c3c3;
  max-width:none;
}

.panel {
  padding: 50px;
  display: none;
  
}
.prikaz{
    padding: 50px;
  display: none;
  padding: 5px;
  text-align: left;
  background-color: #e5eecc;
  border: solid 1px #c3c3c3;
  min-width:600px;
}
</style>
    @endsection