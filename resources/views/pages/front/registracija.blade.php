@extends("layouts/front")

@section("centralniSadrzaj")
<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Registracija</h2>
                            </div>
                            <div id="ispis">
                                
                            </div>
                            
                            <form action="{{ url("/registracija") }}" method="POST">
                            @csrf
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Ime"/>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Prezime"/>
                                    </div>
                                    
                                    <div class="col-12 mb-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Lozinka"/>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="password" class="form-control" name="passwordrepeat" id="passwordrepeat" placeholder="Ponovi Lozinku"/>
                                    </div>

                                </div>
                                <div class="cart-btn mt-100">
                                <input type="button" class="btn amado-btn w-100" value="Registruj se"/>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset("/js/jquery/jquery-2.2.4.min.js")}}"></script>
    <script>
        $(".btn").click(function(){
        
            let fn=document.getElementById("first_name").value;
            
            let ln=document.getElementById("last_name").value;

            let email=document.getElementById("email").value;

            let password=document.getElementById("password").value;

            let passwordre=document.getElementById("passwordrepeat").value;

            $.ajax({
                url: window.location,
                method: "POST",
                data: {
                    _token: $("input[name='_token']").val(),
                    first_name:fn,
                    last_name:ln,
                    email:email,
                    password:password,
                    passwordrepeat:passwordre
                },
                dataType: "json",
                success: function(data){
                    
                    document.getElementById("ispis").innerHTML="Uspesno ste se registrovali";
                    document.getElementById("first_name").value="";
                    document.getElementById("last_name").value="";
                    document.getElementById("email").value="";
                    document.getElementById("password").value="";
                    document.getElementById("passwordrepeat").value="";
                },
                error: function(error,xhr,status){
                   
                    if(error.status==400){
                        document.getElementById("ispis").innerHTML="Takav korisnik vec postoji!";
                    }else{
                        
                    let greske=[error.responseJSON.errors];
                    let ispis="";
                    for(let g of greske){
                        if(g.first_name==undefined){
                            g.first_name="";
                        }
                        if(g.last_name==undefined){
                            g.last_name="";
                        }
                        if(g.email==undefined){
                            g.email="";
                        }
                        if(g.password==undefined){
                            g.password="";
                        }
                        if(g.passwordrepeat==undefined){
                            g.passwordrepeat="";
                        }
                         ispis+=g.first_name+g.last_name+g.email+g.password+g.passwordrepeat;
                    
                    }
                    document.getElementById("ispis").innerHTML=ispis;
                    }
                }
            })
        });
    
    </script>
    @endsection
