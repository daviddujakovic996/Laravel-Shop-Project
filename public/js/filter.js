$(document).ready(function () {
        
        

        
        

           
    filter_data();
        function filter_data() {
           
            var minimum_price = $('#hidden_minimum_price').val();
            var maximum_price = $('#hidden_maximum_price').val();
            var brend = get_filter('brend');
            var pol = get_filter('pol');
            var godina = get_filter('godina');
            var opcije=$( "#sortBydateAndPrice option:selected" ).val();
          
           
            var search=$(".search").val();
            
      

            $.ajax({
                url: window.location,
                method: "POST",
                data: {  _token: $("input[name='_token']").val(), minimum_price: minimum_price, maximum_price: maximum_price, brend: brend, pol: pol, godina: godina ,opcije:opcije,search:search},
                dataType:"json",
                success: function (data) {
                    
                    prikazi(data[0]);
                    prikaziPaginaciju(data[0].slice(-1)[0][0].broj);
                },
                error:function(error){
                    document.getElementById("ispis").innerHTML="<h1>Nema podataka za takav izbor!</h1>";
                }
            });

    }
    
  
        function get_filter(class_name) {
            var filter = [];
            $('.' + class_name + ':checked').each(function () {
                filter.push($(this).val());
            });
            return filter;
        }

        $('.form-check-input').click(function () {
            filter_data();
    });


    $("input[type=number]").keyup(function () {
        filter_data();
    })
    $("#sortBydateAndPrice").change(function(){
       
        filter_data();
    })
    $(".search").keyup(function(){
        filter_data();
      });
            
   
    function prikazi(podaci) {
        
        let ispis = "";
        
        for (let p of podaci) {
            if (p.idPatika == undefined) {
                break;
            }
            let cena=0;
            let popust=0;
            if(p.popust>0){
                cena+=parseInt(p.originalnaCena)-parseInt(p.popust);
                popust=parseInt(p.originalnaCena)+" din";
                
            }else{
                cena+=parseInt(p.originalnaCena);
                popust="nema popusta";
             
            }
            
                ispis += ` 
                <div class="col-12 col-sm-6 col-md-12 col-xl-6">
                    <div class="single-product-wrapper">
                     <!-- Product Image -->
                        <div class="product-img">
                            <img src="img/bg-img/${p.Putanja}"/>
                           <img class="hover-img" src="img/bg-img/${p.Putanja}"/>
                         </div>

                    <!-- Product Description -->
                    <div class="product-description d-flex align-items-center justify-content-between">
                   <!-- Product Meta Data -->
                    <div class="product-meta-data">
                    <div class="line"></div>
       
                    <p class="product-price">${cena} din</p>
                    <del>${popust}</del>

                <a href="patika/${p.idPatika}">
                    <h6>${p.Naziv}</h6>
                 </a>
                </div>
                <!-- Ratings & Cart -->
                <div class="ratings-cart text-right">
        
                 <div class="cart">
                   <a href="patika/${p.idPatika}" data-toggle="tooltip" data-placement="left" title="Dodaj u korpu"><img src="img/core-img/cart.png" alt=""></a>
                 </div>
    </div>
</div>
</div>
</div>
            `;
            
            }
        if (ispis == "") {
            ispis += "<h2>NEMA PODATAKA ZA TAKAV IZBOR!</h2>";
            

        }
        document.getElementById("ispis").innerHTML=ispis;

        
    }

    function prikaziPaginaciju(brojProizvoda) {
        
        let brojPoStrani = 6;
        let ukupnoStranica = parseInt(brojProizvoda) / brojPoStrani;
        let ispis = "<li class='page-item'><a class='page-link' href='#'  data-id='1'> << </a></li>";
      
        for (let i = 1; i < ukupnoStranica + 1; i++) {
            ispis +="<li class='page-item'><a class='page-link' href='#' data-id='"+i+"'>"+ i +" </a></li>";
        }

        ispis += "<li class='page-item'><a class='page-link' href='#'  data-id='"+ Math.round(ukupnoStranica) + "'> >> </a></li>";
     
        document.getElementById("pagination").innerHTML = ispis;

        $(".page-link").click(function (e) {
            e.preventDefault();
            var minimum_price = $('#hidden_minimum_price').val();
            var maximum_price = $('#hidden_maximum_price').val();
            var brend = get_filter('brend');
            var pol = get_filter('pol');
            var godina = get_filter('godina');
            var opcije=$( "#sortBydateAndPrice option:selected" ).val();
            var search=$(".search").val();
            

            var pagin = $(this).data("id");
            var paginUInt = parseInt(pagin);

            $.ajax({
                url: window.location,
                method: "POST",
                data: {_token: $("input[name='_token']").val(), minimum_price: minimum_price, maximum_price: maximum_price, brend: brend, pol: pol, godina: godina, strana: paginUInt,opcije: opcije,search:search},
                dataType: "json",
                success: function (data) {
                
                    prikazi(data[0]);
                
                }
            });
        })
    }

    
});