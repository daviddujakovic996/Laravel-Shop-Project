-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2020 at 02:03 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `patike`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktivnostikorisnika`
--

CREATE TABLE `aktivnostikorisnika` (
  `idAktivnosti` int(255) NOT NULL,
  `putanja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `korisnik` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `vreme` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aktivnostikorisnika`
--

INSERT INTO `aktivnostikorisnika` (`idAktivnosti`, `putanja`, `korisnik`, `vreme`) VALUES
(5, '/', '::1', '2020-03-21 22:38:46'),
(6, 'Po%C4%8Detna', '::1', '2020-03-21 22:38:50'),
(7, '/', '::1', '2020-03-21 22:38:51'),
(8, 'Shop', '::1', '2020-03-21 22:38:52'),
(9, 'Shop', '::1', '2020-03-21 22:38:53'),
(10, 'Po%C4%8Detna', '::1', '2020-03-21 22:38:54'),
(11, '/', '::1', '2020-03-21 22:38:54'),
(12, 'Kontakt', '::1', '2020-03-21 22:38:56'),
(13, 'login', '::1', '2020-03-21 22:39:08'),
(14, 'login', '::1', '2020-03-21 22:39:16'),
(15, '/', '::1', '2020-03-21 22:39:17'),
(16, 'Shop', '::1', '2020-03-21 22:39:23'),
(17, 'Shop', '::1', '2020-03-21 22:39:24'),
(18, 'Shop', '::1', '2020-03-21 22:39:30'),
(19, 'Shop', '::1', '2020-03-21 22:39:31'),
(20, 'Shop', '::1', '2020-03-21 22:39:46'),
(21, 'patika/19', '::1', '2020-03-21 22:39:51'),
(22, 'dodajUKorpu', '::1', '2020-03-21 22:39:53'),
(23, 'patika/19', '::1', '2020-03-21 22:39:53'),
(24, 'korpa', '::1', '2020-03-21 22:40:00'),
(25, 'naruci', '::1', '2020-03-21 22:40:04'),
(26, 'korpa', '::1', '2020-03-21 22:40:04'),
(27, 'logout', '::1', '2020-03-21 22:40:38'),
(28, 'login', '::1', '2020-03-21 22:40:38'),
(29, 'login', '::1', '2020-03-21 22:40:45'),
(30, '/', '::1', '2020-03-21 22:40:45'),
(31, 'adminPanel', '::1', '2020-03-21 22:40:46'),
(32, 'adminPanel/godinaIzdanja', '::1', '2020-03-21 22:50:14'),
(33, 'adminPanel/brend', '::1', '2020-03-21 22:50:26'),
(34, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:52:44'),
(35, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:52:58'),
(36, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:53:18'),
(37, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:53:28'),
(38, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:53:40'),
(39, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:53:46'),
(40, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:54:04'),
(41, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:54:25'),
(42, 'adminPanel/aktivnosti', '::1', '2020-03-21 22:55:50'),
(43, 'adminPanel/aktivnosti', '::1', '2020-03-22 17:43:32'),
(44, '/', '::1', '2020-03-22 17:43:33'),
(45, 'Shop', '::1', '2020-03-22 17:43:44'),
(46, 'Shop', '::1', '2020-03-22 17:43:45'),
(47, 'login', '::1', '2020-03-22 17:43:49'),
(48, 'login', '::1', '2020-03-22 17:43:57'),
(49, '/', '::1', '2020-03-22 17:43:58'),
(50, 'adminPanel', '::1', '2020-03-22 17:43:59'),
(51, 'adminPanel/patike', '::1', '2020-03-22 17:44:09'),
(52, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:44:12'),
(53, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:46:07'),
(54, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:46:08'),
(55, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:47:32'),
(56, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:47:33'),
(57, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:48:38'),
(58, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:48:38'),
(59, 'adminPanel/patike', '::1', '2020-03-22 17:48:46'),
(60, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:48:51'),
(61, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:50:06'),
(62, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:50:06'),
(63, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:50:44'),
(64, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:50:45'),
(65, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:51:27'),
(66, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:51:27'),
(67, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:52:05'),
(68, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:52:06'),
(69, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:52:40'),
(70, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:52:40'),
(71, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:53:44'),
(72, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:53:44'),
(73, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:54:19'),
(74, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:54:19'),
(75, 'adminPanel/brend', '::1', '2020-03-22 17:55:03'),
(76, 'adminPanel/brend', '::1', '2020-03-22 17:55:14'),
(77, 'adminPanel/brend', '::1', '2020-03-22 17:55:14'),
(78, 'adminPanel/brend', '::1', '2020-03-22 17:55:23'),
(79, 'adminPanel/brend', '::1', '2020-03-22 17:55:23'),
(80, 'adminPanel/brend', '::1', '2020-03-22 17:56:39'),
(81, 'adminPanel/brend', '::1', '2020-03-22 17:56:45'),
(82, 'adminPanel/brend', '::1', '2020-03-22 17:56:46'),
(83, 'adminPanel/brend', '::1', '2020-03-22 17:56:53'),
(84, 'adminPanel/brend', '::1', '2020-03-22 17:56:53'),
(85, 'adminPanel/patike', '::1', '2020-03-22 17:59:03'),
(86, 'adminPanel/patike/dodaj', '::1', '2020-03-22 17:59:23'),
(87, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:00:03'),
(88, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:00:06'),
(89, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:01:12'),
(90, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:01:13'),
(91, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:02:39'),
(92, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:02:40'),
(93, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:03:46'),
(94, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:03:46'),
(95, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:04:38'),
(96, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:04:39'),
(97, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:05:34'),
(98, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:05:34'),
(99, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:06:39'),
(100, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:06:39'),
(101, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:07:42'),
(102, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:07:43'),
(103, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:08:39'),
(104, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:08:40'),
(105, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:09:42'),
(106, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:09:42'),
(107, 'adminPanel/brend', '::1', '2020-03-22 18:09:52'),
(108, 'adminPanel/brend', '::1', '2020-03-22 18:10:04'),
(109, 'adminPanel/brend', '::1', '2020-03-22 18:10:05'),
(110, 'adminPanel/patike', '::1', '2020-03-22 18:10:11'),
(111, 'adminPanel/patike', '::1', '2020-03-22 18:10:17'),
(112, 'adminPanel/patike', '::1', '2020-03-22 18:10:26'),
(113, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:10:32'),
(114, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:11:37'),
(115, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:11:37'),
(116, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:12:37'),
(117, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:12:38'),
(118, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:13:27'),
(119, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:13:28'),
(120, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:14:22'),
(121, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:14:22'),
(122, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:16:01'),
(123, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:16:01'),
(124, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:16:55'),
(125, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:16:56'),
(126, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:17:36'),
(127, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:17:36'),
(128, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:21:04'),
(129, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:21:05'),
(130, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:22:01'),
(131, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:22:01'),
(132, 'adminPanel/patike', '::1', '2020-03-22 18:22:09'),
(133, 'adminPanel/patike/izmeni/58', '::1', '2020-03-22 18:22:14'),
(134, 'adminPanel/poruke', '::1', '2020-03-22 18:22:41'),
(135, 'adminPanel/patike', '::1', '2020-03-22 18:22:45'),
(136, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:23:20'),
(137, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:24:18'),
(138, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:24:18'),
(139, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:25:05'),
(140, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:25:06'),
(141, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:26:01'),
(142, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:26:01'),
(143, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:26:43'),
(144, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:26:44'),
(145, 'adminPanel/patike', '::1', '2020-03-22 18:26:50'),
(146, 'adminPanel/patike', '::1', '2020-03-22 18:27:16'),
(147, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:29:15'),
(148, 'adminPanel/brend', '::1', '2020-03-22 18:29:40'),
(149, 'adminPanel/brend', '::1', '2020-03-22 18:29:45'),
(150, 'adminPanel/brend', '::1', '2020-03-22 18:29:46'),
(151, 'adminPanel/patike', '::1', '2020-03-22 18:29:54'),
(152, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:29:57'),
(153, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:30:26'),
(154, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:30:26'),
(155, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:31:26'),
(156, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:31:27'),
(157, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:32:22'),
(158, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:32:23'),
(159, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:33:17'),
(160, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:33:18'),
(161, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:34:17'),
(162, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:34:18'),
(163, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:35:02'),
(164, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:35:02'),
(165, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:35:58'),
(166, 'adminPanel/patike/dodaj', '::1', '2020-03-22 18:35:58'),
(167, 'adminPanel/patike', '::1', '2020-03-22 18:36:11'),
(168, 'adminPanel/patike', '::1', '2020-03-22 18:36:24'),
(169, 'logout', '::1', '2020-03-22 19:20:04'),
(170, 'login', '::1', '2020-03-22 19:20:05'),
(171, 'Po%C4%8Detna', '::1', '2020-03-22 19:20:08'),
(172, '/', '::1', '2020-03-22 19:20:08'),
(173, '/', '::1', '2020-03-22 19:23:35'),
(174, '/', '::1', '2020-03-22 19:24:16'),
(175, 'Shop', '::1', '2020-03-22 19:24:23'),
(176, 'Shop', '::1', '2020-03-22 19:24:23'),
(177, 'Shop', '::1', '2020-03-22 19:24:34'),
(178, 'Shop', '::1', '2020-03-22 19:24:37'),
(179, 'Shop', '::1', '2020-03-22 19:24:40'),
(180, 'Shop', '::1', '2020-03-22 19:24:42'),
(181, 'Shop', '::1', '2020-03-22 19:24:47'),
(182, 'Shop', '::1', '2020-03-22 19:24:48'),
(183, 'Shop', '::1', '2020-03-22 19:24:52'),
(184, 'Shop', '::1', '2020-03-22 19:24:53'),
(185, 'Shop', '::1', '2020-03-22 19:24:57'),
(186, 'Shop', '::1', '2020-03-22 19:24:57'),
(187, 'Shop', '::1', '2020-03-22 19:24:58'),
(188, 'Shop', '::1', '2020-03-22 19:24:58'),
(189, 'Shop', '::1', '2020-03-22 19:25:06'),
(190, 'Shop', '::1', '2020-03-22 19:25:07'),
(191, 'Shop', '::1', '2020-03-22 19:25:11'),
(192, 'Shop', '::1', '2020-03-22 19:25:14'),
(193, 'Shop', '::1', '2020-03-22 19:25:16'),
(194, 'Shop', '::1', '2020-03-22 19:25:22'),
(195, 'Shop', '::1', '2020-03-22 19:25:23'),
(196, 'Shop', '::1', '2020-03-22 19:34:55'),
(197, 'Shop', '::1', '2020-03-22 19:35:21'),
(198, 'Shop', '::1', '2020-03-22 19:35:24'),
(199, 'Shop', '::1', '2020-03-22 19:35:34'),
(200, 'Shop', '::1', '2020-03-22 19:35:36'),
(201, 'Shop', '::1', '2020-03-22 19:35:38'),
(202, 'Shop', '::1', '2020-03-22 19:35:39'),
(203, 'Shop', '::1', '2020-03-22 19:35:44'),
(204, 'Shop', '::1', '2020-03-22 19:35:46'),
(205, 'Shop', '::1', '2020-03-22 19:35:48'),
(206, 'Shop', '::1', '2020-03-22 19:35:51'),
(207, 'Shop', '::1', '2020-03-22 19:35:54'),
(208, 'Shop', '::1', '2020-03-22 19:35:57'),
(209, 'Shop', '::1', '2020-03-22 19:35:58'),
(210, 'Po%C4%8Detna', '::1', '2020-03-22 21:00:42'),
(211, '/', '::1', '2020-03-22 21:00:43'),
(212, 'Shop', '::1', '2020-03-22 21:11:51'),
(213, 'Shop', '::1', '2020-03-22 21:11:56'),
(214, 'Po%C4%8Detna', '::1', '2020-03-22 21:13:54'),
(215, '/', '::1', '2020-03-22 21:13:54'),
(216, 'Shop', '::1', '2020-03-22 21:14:01'),
(217, 'Shop', '::1', '2020-03-22 21:14:02'),
(218, 'patika/37', '::1', '2020-03-22 21:22:43'),
(219, 'Kontakt', '::1', '2020-03-22 21:27:18'),
(220, 'login', '::1', '2020-03-22 21:33:03'),
(221, 'registracija', '::1', '2020-03-22 21:36:59'),
(222, 'registracija', '::1', '2020-03-22 21:40:45'),
(223, 'login', '::1', '2020-03-22 21:40:45'),
(224, 'login', '::1', '2020-03-22 21:41:01'),
(225, 'login', '::1', '2020-03-22 21:41:02'),
(226, 'login', '::1', '2020-03-22 21:41:09'),
(227, 'login', '::1', '2020-03-22 21:41:10'),
(228, 'login', '::1', '2020-03-22 21:41:31'),
(229, 'login', '::1', '2020-03-22 21:41:32'),
(230, 'registracija', '::1', '2020-03-22 21:41:36'),
(231, 'registracija', '::1', '2020-03-22 21:42:05'),
(232, 'registracija', '::1', '2020-03-22 21:42:08'),
(233, 'login', '::1', '2020-03-22 21:42:09'),
(234, 'login', '::1', '2020-03-22 21:42:23'),
(235, '/', '::1', '2020-03-22 21:42:23'),
(236, 'patika/34', '::1', '2020-03-22 21:46:37'),
(237, 'dodajUKorpu', '::1', '2020-03-22 21:46:45'),
(238, 'patika/34', '::1', '2020-03-22 21:46:46'),
(239, 'Moj%20Nalog', '::1', '2020-03-22 21:46:56'),
(240, 'Shop', '::1', '2020-03-22 21:47:02'),
(241, 'Shop', '::1', '2020-03-22 21:47:04'),
(242, 'patika/37', '::1', '2020-03-22 21:47:07'),
(243, 'dodajUKorpu', '::1', '2020-03-22 21:47:11'),
(244, 'patika/37', '::1', '2020-03-22 21:47:12'),
(245, 'Kontakt', '::1', '2020-03-22 21:47:28'),
(246, 'Moj%20Nalog', '::1', '2020-03-22 21:50:29'),
(247, 'Shop', '::1', '2020-03-22 21:55:13'),
(248, 'Shop', '::1', '2020-03-22 21:55:14'),
(249, 'patika/37', '::1', '2020-03-22 21:55:16'),
(250, 'korpa', '::1', '2020-03-22 21:57:35'),
(251, 'logout', '::1', '2020-03-22 22:05:42'),
(252, 'login', '::1', '2020-03-22 22:05:42'),
(253, 'login', '::1', '2020-03-22 22:06:27'),
(254, '/', '::1', '2020-03-22 22:06:27'),
(255, 'adminPanel', '::1', '2020-03-22 22:06:29'),
(256, 'adminPanel/mojNalog', '::1', '2020-03-22 22:10:10'),
(257, 'adminPanel/poruke', '::1', '2020-03-22 22:11:56'),
(258, 'logout', '::1', '2020-03-22 22:12:13'),
(259, 'login', '::1', '2020-03-22 22:12:13'),
(260, 'Kontakt', '::1', '2020-03-22 22:12:23'),
(261, 'Kontakt', '::1', '2020-03-22 22:13:05'),
(262, 'Kontakt', '::1', '2020-03-22 22:13:05'),
(263, 'login', '::1', '2020-03-22 22:13:07'),
(264, 'login', '::1', '2020-03-22 22:13:14'),
(265, '/', '::1', '2020-03-22 22:13:14'),
(266, 'adminPanel', '::1', '2020-03-22 22:13:15'),
(267, 'adminPanel/poruke', '::1', '2020-03-22 22:13:21'),
(268, 'adminPanel/poruke/odgovori/nestooo@gmail.com/26', '::1', '2020-03-22 22:16:13'),
(269, 'adminPanel/aktivnosti', '::1', '2020-03-22 22:19:12'),
(270, 'adminPanel/patike', '::1', '2020-03-22 22:22:15'),
(271, 'adminPanel/patike', '::1', '2020-03-22 22:22:50'),
(272, 'adminPanel/godinaIzdanja', '::1', '2020-03-22 22:28:27'),
(273, 'adminPanel/brend', '::1', '2020-03-22 22:30:08'),
(274, 'adminPanel/korpa', '::1', '2020-03-22 22:33:05'),
(275, 'logout', '::1', '2020-03-22 22:33:20'),
(276, 'login', '::1', '2020-03-22 22:33:20'),
(277, 'login', '::1', '2020-03-22 22:33:29'),
(278, '/', '::1', '2020-03-22 22:33:29'),
(279, 'korpa', '::1', '2020-03-22 22:33:33'),
(280, 'naruci', '::1', '2020-03-22 22:33:35'),
(281, 'korpa', '::1', '2020-03-22 22:33:36'),
(282, 'logout', '::1', '2020-03-22 22:33:39'),
(283, 'login', '::1', '2020-03-22 22:33:40'),
(284, 'login', '::1', '2020-03-22 22:33:50'),
(285, 'login', '::1', '2020-03-22 22:33:50'),
(286, 'login', '::1', '2020-03-22 22:33:57'),
(287, '/', '::1', '2020-03-22 22:33:57'),
(288, 'adminPanel', '::1', '2020-03-22 22:33:57'),
(289, 'adminPanel/korpa', '::1', '2020-03-22 22:34:03');

-- --------------------------------------------------------

--
-- Table structure for table `autor`
--

CREATE TABLE `autor` (
  `idA` int(1) NOT NULL,
  `ImePrezime` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Opis` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `Slika` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `autor`
--

INSERT INTO `autor` (`idA`, `ImePrezime`, `Opis`, `Slika`) VALUES
(1, 'David Dujaković', 'Zovem se David Dujaković i dolazim iz Doboja,Bosna i Hercegovina.Imam 22 godine i student sam Visoke ICT škole. Volim da pratim najnovije parfeme, pa sam odlučio da moj sajt bude na tu temu.U bliskoj budućnosti želim da postanem web programer.', 'autor.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `brend`
--

CREATE TABLE `brend` (
  `idBrend` int(100) NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brend`
--

INSERT INTO `brend` (`idBrend`, `Naziv`) VALUES
(182, 'Adidas'),
(186, 'Converse'),
(188, 'Fila'),
(185, 'New Balance'),
(181, 'Nike'),
(184, 'Puma'),
(183, 'Reebok'),
(187, 'Under Armour');

-- --------------------------------------------------------

--
-- Table structure for table `cena`
--

CREATE TABLE `cena` (
  `idCena` int(100) NOT NULL,
  `originalnaCena` decimal(10,0) DEFAULT NULL,
  `popust` decimal(10,0) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cena`
--

INSERT INTO `cena` (`idCena`, `originalnaCena`, `popust`) VALUES
(18, '10000', '3110'),
(19, '17000', '7000'),
(20, '16400', '4500'),
(21, '14551', '3683'),
(22, '21525', '0'),
(23, '11990', '0'),
(24, '10790', '2500'),
(25, '14390', '0'),
(26, '20490', '0'),
(27, '11590', '0'),
(28, '10490', '1200'),
(29, '13690', '0'),
(30, '15790', '0'),
(31, '15795', '0'),
(32, '10995', '0'),
(33, '7591', '3158'),
(34, '7992', '1998'),
(35, '5592', '1398'),
(36, '12631', '3158'),
(37, '14712', '3678'),
(38, '13591', '3398'),
(39, '13992', '3497'),
(40, '8391', '2097'),
(41, '12990', '0'),
(42, '13990', '0'),
(43, '8392', '2097'),
(44, '11990', '0'),
(45, '8392', '2097'),
(46, '10490', '0'),
(47, '12990', '0'),
(48, '10392', '2598'),
(49, '10392', '2580'),
(50, '10999', '4600'),
(51, '9459', '5100'),
(52, '7591', '6998'),
(53, '9592', '2397');

-- --------------------------------------------------------

--
-- Table structure for table `godinaizdanja`
--

CREATE TABLE `godinaizdanja` (
  `idGodinaIzdanja` int(11) NOT NULL,
  `godinaIzdanja` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `godinaizdanja`
--

INSERT INTO `godinaizdanja` (`idGodinaIzdanja`, `godinaIzdanja`) VALUES
(26, 2018),
(27, 2019),
(28, 2020);

-- --------------------------------------------------------

--
-- Table structure for table `kontakt`
--

CREATE TABLE `kontakt` (
  `idPoruka` int(100) NOT NULL,
  `Naslov` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Poruka` text COLLATE utf8_unicode_ci NOT NULL,
  `FromEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ToEmail` int(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kontakt`
--

INSERT INTO `kontakt` (`idPoruka`, `Naslov`, `Poruka`, `FromEmail`, `ToEmail`, `status`) VALUES
(26, 'Poruka adminu', 'Neka poruka adminu', 'nestooo@gmail.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `idKorisnik` int(200) NOT NULL,
  `ImePrezime` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Lozinka` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idUloga` int(10) NOT NULL DEFAULT 2,
  `status` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`idKorisnik`, `ImePrezime`, `Email`, `Lozinka`, `idUloga`, `status`) VALUES
(1, 'AdminAdministrirovic', 'admin@hotmail.com', '2dce020ae12fdbf801f4993fe1acd067', 1, 0),
(5, 'David Dujakoviccc', 'david@hotmaial.com', 'c43ad87223854673395aa9d3efd2a2a7', 2, 0),
(6, 'Mika Mikic', 'nesto@hotmail.com', '28b0717d03cf7ab20cc25dc743c60676', 2, 0),
(9, 'Neko Nekic', 'neko@gmail.com', 'cc96d05fd5e74bcf6d1d0249a863954f', 2, 0),
(13, 'Beee Beeee', 'pera@gmail.comm', '1adbb3178591fd5bb0c248518f39bf6d', 2, 0),
(51, 'Uros Vasiljevic', 'uros@gmail.com', 'd0aeeef9a9aeddbaa999b7b65101b3a1', 2, 0),
(52, 'Pera Peric', 'test@gmail.com', 'd0aeeef9a9aeddbaa999b7b65101b3a1', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `korpa`
--

CREATE TABLE `korpa` (
  `IdKorpa` int(100) NOT NULL,
  `IdKorisnik` int(200) NOT NULL,
  `IdPatika` int(100) NOT NULL,
  `Kolicina` int(10) NOT NULL,
  `UkupnaCena` int(100) NOT NULL,
  `Narucen` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korpa`
--

INSERT INTO `korpa` (`IdKorpa`, `IdKorisnik`, `IdPatika`, `Kolicina`, `UkupnaCena`, `Narucen`) VALUES
(105, 52, 34, 2, 20000, 1),
(106, 52, 37, 2, 43050, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(10) NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idMenu`, `Naziv`) VALUES
(3, 'Kontakt'),
(7, 'Moj Nalog'),
(1, 'Početna'),
(2, 'Shop');

-- --------------------------------------------------------

--
-- Table structure for table `patike`
--

CREATE TABLE `patike` (
  `idPatika` int(100) NOT NULL,
  `idBrend` int(100) NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idPol` int(10) NOT NULL,
  `idGodinaIzdanja` int(4) NOT NULL,
  `idSlika` int(100) NOT NULL DEFAULT 11,
  `idCena` int(100) NOT NULL,
  `status` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patike`
--

INSERT INTO `patike` (`idPatika`, `idBrend`, `Naziv`, `idPol`, `idGodinaIzdanja`, `idSlika`, `idCena`, `status`) VALUES
(33, 182, 'ADIDAS PATIKE RIVALRY LOW', 1, 27, 29, 18, 0),
(34, 182, 'ADIDAS PATIKE SUPERSTAR 80S RECON', 1, 26, 30, 19, 0),
(35, 182, 'ADIDAS PATIKE BOSTONSUPERXR1', 1, 27, 31, 20, 0),
(36, 181, 'NIKE PATIKE AIR MAX 90 20', 1, 27, 32, 21, 0),
(37, 181, 'NIKE PATIKE W AIR VAPORMAX FLYKNIT 3', 2, 28, 33, 22, 0),
(38, 183, 'REEBOK PATIKE CL LTHR REE', 2, 28, 34, 23, 0),
(39, 183, 'REEBOK PATIKE CLUB C REE', 2, 27, 35, 24, 0),
(40, 182, 'ADIDAS PATIKE OZWEEGO', 1, 27, 36, 25, 0),
(41, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M', 1, 28, 37, 26, 0),
(42, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE W 574', 2, 27, 38, 27, 0),
(43, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M 574', 1, 26, 39, 28, 0),
(44, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M 997', 1, 28, 40, 29, 0),
(45, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M 997S', 1, 27, 41, 30, 0),
(46, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M 997S', 1, 27, 42, 31, 0),
(47, 185, 'NEW BALANCE PATIKE PATIKE NEW BALANCE M 997', 2, 27, 43, 32, 0),
(48, 186, 'CONVERSE PATIKE CHUCK 70', 2, 26, 44, 33, 0),
(49, 186, 'CONVERSE PATIKE CHUCK TAYLOR ALL STAR PLATFORM EVA', 2, 26, 45, 34, 0),
(50, 186, 'CONVERSE PATIKE ALL STAR', 2, 27, 46, 35, 0),
(51, 187, 'UNDER ARMOUR PATIKE UA HOVR SUMMIT', 1, 27, 47, 36, 0),
(52, 187, 'UNDER ARMOUR PATIKE UA HOVR PHANTOM SE TREK', 1, 28, 48, 37, 0),
(53, 187, 'UNDER ARMOUR PATIKE UA W HOVR PHANTOM SE TREK', 2, 28, 49, 38, 0),
(54, 187, 'UNDER ARMOUR PATIKE UA HOVR PHANTOM RN', 1, 28, 50, 39, 0),
(55, 184, 'PUMA PATIKE PUMA LIA POP WNS', 2, 27, 51, 40, 0),
(56, 184, 'PUMA PATIKE PUMA RSX PUZZLE', 1, 28, 52, 41, 0),
(57, 184, 'PUMA PATIKE PUMA MAPM RSCUBE', 1, 28, 53, 42, 0),
(58, 184, 'PUMA PATIKE PUMA LIA POP WNSSS', 2, 27, 54, 43, 0),
(59, 184, 'PUMA PATIKE PUMA CALI SPORT MIX WNS', 2, 27, 55, 44, 0),
(60, 184, 'PUMA PATIKE PUMA RS TRAIL', 1, 26, 56, 45, 0),
(61, 184, 'PUMA PATIKE PUMA CALI WNS', 2, 28, 57, 46, 0),
(62, 188, 'FILA PATIKE DISRUPTOR ANIMAL WMN', 2, 28, 58, 47, 0),
(63, 188, 'FILA PATIKE DFORMATION WMN', 2, 26, 59, 48, 0),
(64, 188, 'FILA PATIKE DISRUPTOR LOGO LOW WMN', 2, 27, 60, 49, 0),
(65, 188, 'FILA PATIKE DISRUPTOR LOW WMN', 2, 26, 61, 50, 0),
(66, 188, 'FILA PATIKE DISRUPTOR PLOW WMN', 1, 26, 62, 51, 0),
(67, 188, 'FILA PATIKE DISRUPTORP LOW WMN', 1, 26, 63, 52, 0),
(68, 188, 'FILA PATIKE FILA DISRUPTOR LOW WMN', 2, 27, 64, 53, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pol`
--

CREATE TABLE `pol` (
  `idPol` int(10) NOT NULL,
  `Naziv` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pol`
--

INSERT INTO `pol` (`idPol`, `Naziv`) VALUES
(1, 'Muški'),
(2, 'Ženski');

-- --------------------------------------------------------

--
-- Table structure for table `slika`
--

CREATE TABLE `slika` (
  `idSlika` int(100) NOT NULL,
  `Putanja` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1.jpg',
  `NazivSlike` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'test',
  `NovaSlika` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slika`
--

INSERT INTO `slika` (`idSlika`, `Putanja`, `NazivSlike`, `NovaSlika`) VALUES
(1, '1584660537.jpeg', 'Aberccrombie', 'parfem-abercrombie-fitch-fierce-cologne-edc-100ml-088.jpg'),
(2, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'Cartier', 'parfem-cartier-declaration-edp-100ml-7e6.jpg'),
(3, '1584660684.jpeg', 'azzaro', 'parfem-azzaro-visit-for-women-edp-75ml-939.jpg'),
(4, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'ABERCROMBIE & FITCH FIRST INSTINCT WOgfggfMANN', 'image002.jpg'),
(5, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'pocetna', '0'),
(6, '1584660428.jpeg', 'aramis', 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg'),
(7, '1584660456.jpeg', 'acqua di parma', 'parfem-acqua-di-parma-colonia-intensa-edc-100ml-228.jpg'),
(11, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'nestooooo', 'vest4.jpg'),
(12, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'shop', '0'),
(13, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'burberry-brit-edp-100ml', 'parfem-burberry-brit-edp-100ml-14f.jpg'),
(14, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'bvlgari_blv_notte_pour_femme_edp_75ml', 'parfem-bvlgari-blv-notte-pour-femme-edp-75ml-de7.jpg.jpg'),
(15, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'britney_spears_midnight_fantasy_edp_100ml', 'parfem-britney-spears-midnight-fantasy-edp-100ml-43a.jpg'),
(16, '1584660616.jpeg', 'britney_spears_fantasy_edp_100ml_2', 'parfem-britney-spears-fantasy-edp-100ml-5bf.jpg'),
(17, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'n', '0'),
(18, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'image012.jpg', '0'),
(19, '1584660348.jpeg', 'test', '0'),
(20, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'test', '0'),
(21, '1584660300.jpeg', 'test', '0'),
(22, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'test', '0'),
(23, 'parfem-aramis-900-herbal-cologne-100ml-b04.jpg', 'test', '0'),
(24, '1584661755.jpeg', 'test', '0'),
(25, '1584661915.jpeg', 'test', '0'),
(26, '1584662352.jpeg', 'test', '0'),
(27, '1584729808.jpeg', 'test', '0'),
(28, 'EE4961_800_800px.jpg', 'test', '0'),
(29, '1584899167.jpeg', 'test', '0'),
(30, '1584899252.jpeg', 'test', '0'),
(31, '1584899318.jpeg', 'test', '0'),
(32, '1584899406.jpeg', 'test', '0'),
(33, '1584899487.jpeg', 'test', '0'),
(34, '1584899560.jpeg', 'test', '0'),
(35, '1584899624.jpeg', 'test', '0'),
(36, '1584899659.jpeg', 'test', '0'),
(37, '1584900005.jpeg', 'test', '0'),
(38, '1584900072.jpeg', 'test', '0'),
(39, '1584900159.jpeg', 'test', '0'),
(40, '1584900226.jpeg', 'test', '0'),
(41, '1584900279.jpeg', 'test', '0'),
(42, '1584900334.jpeg', 'test', '0'),
(43, '1584900399.jpeg', 'test', '0'),
(44, '1584900462.jpeg', 'test', '0'),
(45, '1584900519.jpeg', 'test', '0'),
(46, '1584900582.jpeg', 'test', '0'),
(47, '1584900697.jpeg', 'test', '0'),
(48, '1584900757.jpeg', 'test', '0'),
(49, '1584900808.jpeg', 'test', '0'),
(50, '1584900862.jpeg', 'test', '0'),
(51, '1584900961.jpeg', 'test', '0'),
(52, '1584901015.jpeg', 'test', '0'),
(53, '1584901056.jpeg', 'test', '0'),
(54, '1.jpg', 'test', '0'),
(55, '1584901505.jpeg', 'test', '0'),
(56, '1584901561.jpeg', 'test', '0'),
(57, '1584901603.jpeg', 'test', '0'),
(58, '1584901826.jpeg', 'test', '0'),
(59, '1584901886.jpeg', 'test', '0'),
(60, '1584901943.jpeg', 'test', '0'),
(61, '1584901997.jpeg', 'test', '0'),
(62, '1584902057.jpeg', 'test', '0'),
(63, '1584902102.jpeg', 'test', '0'),
(64, '1584902158.jpeg', 'test', '0');

-- --------------------------------------------------------

--
-- Table structure for table `uloga`
--

CREATE TABLE `uloga` (
  `idUloga` int(10) NOT NULL,
  `NazivUloge` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uloga`
--

INSERT INTO `uloga` (`idUloga`, `NazivUloge`) VALUES
(1, 'Admin'),
(2, 'Korisnik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivnostikorisnika`
--
ALTER TABLE `aktivnostikorisnika`
  ADD PRIMARY KEY (`idAktivnosti`);

--
-- Indexes for table `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`idA`);

--
-- Indexes for table `brend`
--
ALTER TABLE `brend`
  ADD PRIMARY KEY (`idBrend`),
  ADD UNIQUE KEY `Naziv` (`Naziv`);

--
-- Indexes for table `cena`
--
ALTER TABLE `cena`
  ADD PRIMARY KEY (`idCena`);

--
-- Indexes for table `godinaizdanja`
--
ALTER TABLE `godinaizdanja`
  ADD PRIMARY KEY (`idGodinaIzdanja`),
  ADD UNIQUE KEY `godinaIzdanja` (`godinaIzdanja`);

--
-- Indexes for table `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`idPoruka`),
  ADD KEY `ToEmail` (`ToEmail`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`idKorisnik`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `idUloga` (`idUloga`);

--
-- Indexes for table `korpa`
--
ALTER TABLE `korpa`
  ADD PRIMARY KEY (`IdKorpa`),
  ADD KEY `IdKorisnik` (`IdKorisnik`),
  ADD KEY `korpa_ibfk_2` (`IdPatika`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`),
  ADD UNIQUE KEY `Naziv` (`Naziv`);

--
-- Indexes for table `patike`
--
ALTER TABLE `patike`
  ADD PRIMARY KEY (`idPatika`),
  ADD KEY `idGodinaIzdanja` (`idGodinaIzdanja`),
  ADD KEY `idPol` (`idPol`),
  ADD KEY `idSlika` (`idSlika`),
  ADD KEY `idBrend` (`idBrend`),
  ADD KEY `idCena` (`idCena`);

--
-- Indexes for table `pol`
--
ALTER TABLE `pol`
  ADD PRIMARY KEY (`idPol`);

--
-- Indexes for table `slika`
--
ALTER TABLE `slika`
  ADD PRIMARY KEY (`idSlika`);

--
-- Indexes for table `uloga`
--
ALTER TABLE `uloga`
  ADD PRIMARY KEY (`idUloga`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivnostikorisnika`
--
ALTER TABLE `aktivnostikorisnika`
  MODIFY `idAktivnosti` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `autor`
--
ALTER TABLE `autor`
  MODIFY `idA` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `brend`
--
ALTER TABLE `brend`
  MODIFY `idBrend` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `cena`
--
ALTER TABLE `cena`
  MODIFY `idCena` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `godinaizdanja`
--
ALTER TABLE `godinaizdanja`
  MODIFY `idGodinaIzdanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `kontakt`
--
ALTER TABLE `kontakt`
  MODIFY `idPoruka` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `idKorisnik` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `korpa`
--
ALTER TABLE `korpa`
  MODIFY `IdKorpa` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `patike`
--
ALTER TABLE `patike`
  MODIFY `idPatika` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `pol`
--
ALTER TABLE `pol`
  MODIFY `idPol` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slika`
--
ALTER TABLE `slika`
  MODIFY `idSlika` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `uloga`
--
ALTER TABLE `uloga`
  MODIFY `idUloga` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kontakt`
--
ALTER TABLE `kontakt`
  ADD CONSTRAINT `kontakt_ibfk_1` FOREIGN KEY (`ToEmail`) REFERENCES `korisnici` (`idKorisnik`);

--
-- Constraints for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD CONSTRAINT `korisnici_ibfk_1` FOREIGN KEY (`idUloga`) REFERENCES `uloga` (`idUloga`);

--
-- Constraints for table `korpa`
--
ALTER TABLE `korpa`
  ADD CONSTRAINT `korpa_ibfk_1` FOREIGN KEY (`IdKorisnik`) REFERENCES `korisnici` (`idKorisnik`),
  ADD CONSTRAINT `korpa_ibfk_2` FOREIGN KEY (`IdPatika`) REFERENCES `patike` (`idPatika`) ON DELETE CASCADE;

--
-- Constraints for table `patike`
--
ALTER TABLE `patike`
  ADD CONSTRAINT `patike_ibfk_2` FOREIGN KEY (`idGodinaIzdanja`) REFERENCES `godinaizdanja` (`idGodinaIzdanja`) ON DELETE CASCADE,
  ADD CONSTRAINT `patike_ibfk_3` FOREIGN KEY (`idPol`) REFERENCES `pol` (`idPol`),
  ADD CONSTRAINT `patike_ibfk_4` FOREIGN KEY (`idSlika`) REFERENCES `slika` (`idSlika`),
  ADD CONSTRAINT `patike_ibfk_5` FOREIGN KEY (`idBrend`) REFERENCES `brend` (`idBrend`) ON DELETE CASCADE,
  ADD CONSTRAINT `patike_ibfk_7` FOREIGN KEY (`idCena`) REFERENCES `cena` (`idCena`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
